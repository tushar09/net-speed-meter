package triumphit.nets.speeds.meters;

/**
 * Created by Tushar on 1/23/2016.
 */
public interface OnComplete {
    public void onDownloadComplete(String result);
}
