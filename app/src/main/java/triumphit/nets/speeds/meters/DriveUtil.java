package triumphit.nets.speeds.meters;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;

/**
 * Created by Tushar on 12/27/2015.
 */
public class DriveUtil implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    private Context context;

    private static final int REQUEST_CODE_CAPTURE_IMAGE = 1;
    private static final int REQUEST_CODE_CREATOR = 2;
    private static final int REQUEST_CODE_RESOLUTION = 3;


    public DriveUtil(Context context) {
        this.context = context;
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_FILE)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    void connect(){

    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.e("asdf", "API client connected.");
//        if (mBitmapToSave == null) {
//            // This activity has no UI of its own. Just start the camera.
//            Toast.makeText(Gdriveactivity.this, "connected", 1000).show();
//            //startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE),
//            //   REQUEST_CODE_CAPTURE_IMAGE);
//            //   return;
//
//        }
        //insertFile();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
// Called whenever the API client fails to connect.
        Log.e("Failed", "GoogleApiClient connection failed: " + result.toString());
        //Toast.makeText(Gdriveactivity.this, "connected failed", 1000).show();
        if (!result.hasResolution()) {
            // show the localized error dialog.
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), (Activity) context, 0).show();
            return;
        }

// Called typically when the app is not yet authorized,
// authorization
// dialog is displayed to the user.
        try {
            result.startResolutionForResult((Activity) context, REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException e) {
            Log.e("Testing drive", "Exception while starting resolution activity", e);
        }
    }
}
