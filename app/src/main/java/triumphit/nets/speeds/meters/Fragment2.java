package triumphit.nets.speeds.meters;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Fragment2 extends Fragment {

    View v;
    boolean tx = true;

    private static Handler handler;
    private static Runnable updateTask;
    private boolean handlerCheck = false;
    static boolean b;
    AppWidgetManager awm;
    RemoteViews rv;
    ComponentName thisWidget;
    ConnectivityManager connMgr;
    android.net.NetworkInfo wifi, mobile;
    String name;
    WifiInfo wifiInfo;
    WifiManager wifiMgr;
    Intent i;
    static int k = 200;
    ProgressBar pb;
    ArrayList speed;
    TextView tv;
    Button setLimit, totalUsed;
    int t = 0;
    LineChart mChart;
    Random r;

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    private float[] yData = {80, 20};
    private String[] xData = {"Used 80%", "Available 20%"};
    ProgressBar pBar;
    private TextView info;
    View mCustomView;
    Tracker realTime;
    TextView used, availavle, dayLeft;

    private Context context;
    String accountName;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        sp = getActivity().getSharedPreferences("stat", Context.MODE_PRIVATE);
        editor = sp.edit();

        final TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(getActivity());

        Account account = getAccount(AccountManager.get(getActivity().getApplicationContext()));
        if (account == null) {
            accountName = "notSet";
        } else {
            accountName = account.name;
        }
        context = getContext();
        realTime = ((Analytics) getActivity().getApplication()).getTracker(Analytics.TrackerName.APP_TRACKER);
        realTime.setScreenName("Wifi View");
        realTime.send(new HitBuilders.AppViewBuilder().build());
        realTime.enableAdvertisingIdCollection(true);
        realTime.enableExceptionReporting(true);
        realTime.setAnonymizeIp(true);

        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fixed, container, false);
        setLimit = (Button) v.findViewById(R.id.button);
        totalUsed = (Button) v.findViewById(R.id.button2);

        pBar = (ProgressBar) v.findViewById(R.id.progressBar);
        used = (TextView) v.findViewById(R.id.textView14);
        dayLeft = (TextView) v.findViewById(R.id.textView15);

        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

        try {
            dayLeft.setText(getTimeDiff());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        availavle = (TextView) v.findViewById(R.id.textView13);
        setLimit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), Limii.class));
                realTime.send(new HitBuilders.EventBuilder()
                        .setCategory("Set Plan from Wifi")
                        .setAction("Set Plan from Wifi")
                        .setLabel("Set Plan from Wifi")
                        .build());
            }
        });
        totalUsed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), Details.class));
                realTime.send(new HitBuilders.EventBuilder()
                        .setCategory("History from Wifi")
                        .setAction("History from Wifi")
                        .setLabel("History from Wifi")
                        .build());
            }
        });

        calculateAvailability();
        //setPieChart();

        mChart = (LineChart) v.findViewById(R.id.chart);


        mChart.setDescription("");
        mChart.setNoDataTextDescription("You need to provide data for the chart.");
        mChart.setTouchEnabled(true);
        r = new Random();
        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);
        mChart.setPinchZoom(true);

        mChart.setBackgroundColor(Color.TRANSPARENT);

        LineData data = new LineData();
        data.setValueTextColor(Color.parseColor("#00A5EC"));
        mChart.setData(data);
        Legend l = mChart.getLegend();

        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
        l.setForm(Legend.LegendForm.LINE);
        //l.setTypeface(tf);
        l.setTextColor(Color.parseColor("#00A5EC"));
        XAxis xl = mChart.getXAxis();

        //xl.setTypeface(tf);
        xl.setTextColor(Color.parseColor("#00A5EC"));
        xl.setDrawGridLines(true);
        xl.setAvoidFirstLastClipping(true);
        xl.setSpaceBetweenLabels(10);
        xl.setEnabled(true);

        YAxis leftAxis = mChart.getAxisLeft();
        //leftAxis.setTypeface(tf);
        leftAxis.setTextColor(Color.parseColor("#00A5EC"));
        //leftAxis.setAxisMaxValue(100f);
        leftAxis.setAxisMinValue(0f);
        leftAxis.setStartAtZero(false);
        leftAxis.setDrawGridLines(true);
        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);
        tx = true;
        feedMultiple();


        return v;
    }

    String getTimeDiff() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());

        String dateStart = format.format(c.getTime());
        String dateStop = sp.getString("dataDuration_wifi", "0/0/0") + " 00:00:00";
        //Log.e("Date", dateStop);
        //Log.e("Date2", format.format(c.getTime()));

        // Custom date format


        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Get msec from each, and subtract.
        long diff = d2.getTime() - d1.getTime();

        //int day = (int)TimeUnit.SECONDS.toDays(diff);
        //long hours = TimeUnit.SECONDS.toHours(diff) - (day *24);
        //long minute = TimeUnit.SECONDS.toMinutes(diff) - (TimeUnit.SECONDS.toHours(diff)* 60);
        //long second = TimeUnit.SECONDS.toSeconds(diff) - (TimeUnit.SECONDS.toMinutes(diff) *60);

        //long diffSeconds = diff / 1000 % 60;
        //long diffMinutes = diff / (60 * 1000) % 60;
        //long diffHours = diff / (60 * 60 * 1000) % 24;
        //long diffDays = diff / (60 * 60 * 1000 * 24);
        return printDifference(diff);
    }

    public String printDifference(long different) {
        if (!sp.getBoolean("limitless_wifi", false)) {
            if (different < 0) {
                return "Expired";
            }

            //milliseconds
            //long different = endDate.getTime() - startDate.getTime();

            //System.out.println("startDate : " + startDate);
            //System.out.println("endDate : "+ endDate);
            //System.out.println("different : " + different);

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;
            return "Time left " + String.format("%d:%d:%d:%d%n", elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        } else {
            return "Infinity";
        }

    }

    private void calculateAvailability() {
        long limit = sp.getLong("dataLimit_wifi", 0) / 1000;
        long used = sp.getLong("totalWifi", 0);
        if (sp.getLong("dataLimit_wifi", 0) < 0) {
            availavle.setText("Unused: Unlimited");
            if (used > 1000) {
                double d = used / 1000.0;
                d = d * 100;
                d = Math.round(d);
                d = d / 100;
                this.used.setText("Used: " + d + "GB");
            } else {
                this.used.setText("Used: " + used + "MB");
            }
        } else {
            pBar.setProgress((int) getPercentage(used, limit));

            if ((limit - used) > 1000) {
                double d = (limit - used) / 1000.0;
                d = d * 100;
                d = Math.round(d);
                d = d / 100;
                availavle.setText("Unused: " + d + "GB");
            } else {
                availavle.setText("Unused: " + (limit - used) + "MB");
            }

            //availavle.setText("Available: " + getPercentage((limit - used), limit)+ "%");
            if (used > 1000) {
                double d = used / 1000.0;
                d = d * 100;
                d = Math.round(d);
                d = d / 100;
                this.used.setText("Used: " + d + "GB");
            } else {
                this.used.setText("Used: " + used + "MB");
            }

            //Log.e("limit - used", "" + (limit - used));
        }
    }

    public static float getPercentage(long n, long total) {
        float proportion = ((float) n) / ((float) total);
        return proportion * 100;
    }

    private void addEntry() {
        try {
            dayLeft.setText(getTimeDiff());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        LineData data = mChart.getData();

        if (data != null) {

            LineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            float t = (float) sp.getLong("current_wifi", 0);
            String unit = "KB/s";
            if (t > 999) {
                t = (t / (float) 1024);
                unit = "MB/s";
            }
            data.addXValue(String.format("%.1f", t) + unit);
            data.addEntry(new Entry(sp.getLong("current_wifi", 0), set.getEntryCount()), 0);
            //Log.e("test", "" + );

            // let the chart know it's data has changed
            mChart.notifyDataSetChanged();

            // limit the number of visible entries
            mChart.setVisibleXRangeMaximum(10);

            // mChart.setVisibleYRange(30, AxisDependency.LEFT);

            // move to the latest entry
            mChart.moveViewToX(data.getXValCount() - 11);

            // this automatically refreshes the chart (calls invalidate())
            // mChart.moveViewTo(data.getXValCount()-7, 55f,
            // AxisDependency.LEFT);
            calculateAvailability();
        }
    }

    private LineDataSet createSet() {

        LineDataSet set = new LineDataSet(null, "Real Time Speed(up + down) KB/s");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(Color.parseColor("#00A5EC"));
        set.setCircleColor(Color.parseColor("#FF0A89"));
        set.setLineWidth(4f);
        set.setCircleSize(2f);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.parseColor("#00A5EC"));
        set.setValueTextSize(11f);
        //set.setDrawValues(true);
        return set;
    }

    private void feedMultiple() {

        new Thread(new Runnable() {

            @Override
            public void run() {
                while (tx) {

                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            addEntry();
                        }
                    });

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tx = false;
    }

    public Account getAccount(AccountManager accountManager) {
        Account account = null;
        if (ContextCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.GET_ACCOUNTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    android.Manifest.permission.GET_ACCOUNTS)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{android.Manifest.permission.GET_ACCOUNTS},
                        99);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else {
            Account[] accounts = accountManager.getAccountsByType("com.google");

            if (accounts.length > 0) {
                account = accounts[0];
            } else {
                account = null;
            }
        }

        return account;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Account account = getAccount(AccountManager.get(getActivity().getApplicationContext()));
            if (account == null) {
                accountName = "notSet";
            } else {
                accountName = account.name;
            }
        }
    }
}
