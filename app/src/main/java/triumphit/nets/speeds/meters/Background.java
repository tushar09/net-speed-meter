package triumphit.nets.speeds.meters;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Background extends Service {
    public static Notification notification;
    NotificationCompat.Builder builder;
    private static Handler handler;
    private static Runnable updateTask;
    private static boolean handlerCheck = false;
    static boolean b;
    AppWidgetManager awm;
    RemoteViews rv;
    ComponentName thisWidget;
    ConnectivityManager connMgr;
    NetworkInfo wifi, mobile;
    String name;
    WifiInfo wifiInfo;
    WifiManager wifiMgr;
    PendingIntent pendingIntent;
    static long up1 = 1, up2 = 1, dn1 = 1, dn2 = 1;
    NotificationManager nm;
    PendingIntent contentIntent;
    ArrayList<Integer> speed = new ArrayList<Integer>();;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    static long totalDownWifi;
    static long totalUpWifi;
    static long totalDownWifi1;
    static long totalUpWifi1;
    static long totalWifi;
    static long totalDownMob;
    static long totalUpMob;
    static long totalDownMob1;
    static long totalUpMob1;
    static long totalMob;
    static long current;
    static long finalTotal;
    long finalTotalSub1;
    long finalTotalSub2;
    Context context;

    public static boolean firstTime = false;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        startInBackground();
        contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, Landing_Page.class), 0);
        sp = getSharedPreferences("stat", Context.MODE_PRIVATE);
        editor = sp.edit();

        //speed = new ArrayList<Integer>();
        speed.add(R.drawable.image000);
        speed.add(R.drawable.image001);
        speed.add(R.drawable.image002);
        speed.add(R.drawable.image003);
        speed.add(R.drawable.image004);
        speed.add(R.drawable.image005);
        speed.add(R.drawable.image006);
        speed.add(R.drawable.image007);
        speed.add(R.drawable.image008);
        speed.add(R.drawable.image009);
        speed.add(R.drawable.image010);
        speed.add(R.drawable.image011);
        speed.add(R.drawable.image012);
        speed.add(R.drawable.image013);
        speed.add(R.drawable.image014);
        speed.add(R.drawable.image015);
        speed.add(R.drawable.image016);
        speed.add(R.drawable.image017);
        speed.add(R.drawable.image018);
        speed.add(R.drawable.image019);
        speed.add(R.drawable.image020);
        speed.add(R.drawable.image021);
        speed.add(R.drawable.image022);
        speed.add(R.drawable.image023);
        speed.add(R.drawable.image024);
        speed.add(R.drawable.image025);
        speed.add(R.drawable.image026);
        speed.add(R.drawable.image027);
        speed.add(R.drawable.image028);
        speed.add(R.drawable.image029);
        speed.add(R.drawable.image030);
        speed.add(R.drawable.image031);
        speed.add(R.drawable.image032);
        speed.add(R.drawable.image033);
        speed.add(R.drawable.image034);
        speed.add(R.drawable.image035);
        speed.add(R.drawable.image036);
        speed.add(R.drawable.image037);
        speed.add(R.drawable.image038);
        speed.add(R.drawable.image039);
        speed.add(R.drawable.image040);
        speed.add(R.drawable.image041);
        speed.add(R.drawable.image042);
        speed.add(R.drawable.image043);
        speed.add(R.drawable.image044);
        speed.add(R.drawable.image045);
        speed.add(R.drawable.image046);
        speed.add(R.drawable.image047);
        speed.add(R.drawable.image048);
        speed.add(R.drawable.image049);
        speed.add(R.drawable.image050);
        speed.add(R.drawable.image051);
        speed.add(R.drawable.image052);
        speed.add(R.drawable.image053);
        speed.add(R.drawable.image054);
        speed.add(R.drawable.image055);
        speed.add(R.drawable.image056);
        speed.add(R.drawable.image057);
        speed.add(R.drawable.image058);
        speed.add(R.drawable.image059);
        speed.add(R.drawable.image060);
        speed.add(R.drawable.image061);
        speed.add(R.drawable.image062);
        speed.add(R.drawable.image063);
        speed.add(R.drawable.image064);
        speed.add(R.drawable.image065);
        speed.add(R.drawable.image066);
        speed.add(R.drawable.image067);
        speed.add(R.drawable.image068);
        speed.add(R.drawable.image069);
        speed.add(R.drawable.image070);
        speed.add(R.drawable.image071);
        speed.add(R.drawable.image072);
        speed.add(R.drawable.image073);
        speed.add(R.drawable.image074);
        speed.add(R.drawable.image075);
        speed.add(R.drawable.image076);
        speed.add(R.drawable.image077);
        speed.add(R.drawable.image078);
        speed.add(R.drawable.image079);
        speed.add(R.drawable.image080);
        speed.add(R.drawable.image081);
        speed.add(R.drawable.image082);
        speed.add(R.drawable.image083);
        speed.add(R.drawable.image084);
        speed.add(R.drawable.image085);
        speed.add(R.drawable.image086);
        speed.add(R.drawable.image087);
        speed.add(R.drawable.image088);
        speed.add(R.drawable.image089);
        speed.add(R.drawable.image090);
        speed.add(R.drawable.image091);
        speed.add(R.drawable.image092);
        speed.add(R.drawable.image093);
        speed.add(R.drawable.image094);
        speed.add(R.drawable.image095);
        speed.add(R.drawable.image096);
        speed.add(R.drawable.image097);
        speed.add(R.drawable.image098);
        speed.add(R.drawable.image099);
        speed.add(R.drawable.image100);
        speed.add(R.drawable.image101);
        speed.add(R.drawable.image102);
        speed.add(R.drawable.image103);
        speed.add(R.drawable.image104);
        speed.add(R.drawable.image105);
        speed.add(R.drawable.image106);
        speed.add(R.drawable.image107);
        speed.add(R.drawable.image108);
        speed.add(R.drawable.image109);
        speed.add(R.drawable.image110);
        speed.add(R.drawable.image111);
        speed.add(R.drawable.image112);
        speed.add(R.drawable.image113);
        speed.add(R.drawable.image114);
        speed.add(R.drawable.image115);
        speed.add(R.drawable.image116);
        speed.add(R.drawable.image117);
        speed.add(R.drawable.image118);
        speed.add(R.drawable.image119);
        speed.add(R.drawable.image120);
        speed.add(R.drawable.image121);
        speed.add(R.drawable.image122);
        speed.add(R.drawable.image123);
        speed.add(R.drawable.image124);
        speed.add(R.drawable.image125);
        speed.add(R.drawable.image126);
        speed.add(R.drawable.image127);
        speed.add(R.drawable.image128);
        speed.add(R.drawable.image129);
        speed.add(R.drawable.image130);
        speed.add(R.drawable.image131);
        speed.add(R.drawable.image132);
        speed.add(R.drawable.image133);
        speed.add(R.drawable.image134);
        speed.add(R.drawable.image135);
        speed.add(R.drawable.image136);
        speed.add(R.drawable.image137);
        speed.add(R.drawable.image138);
        speed.add(R.drawable.image139);
        speed.add(R.drawable.image140);
        speed.add(R.drawable.image141);
        speed.add(R.drawable.image142);
        speed.add(R.drawable.image143);
        speed.add(R.drawable.image144);
        speed.add(R.drawable.image145);
        speed.add(R.drawable.image146);
        speed.add(R.drawable.image147);
        speed.add(R.drawable.image148);
        speed.add(R.drawable.image149);
        speed.add(R.drawable.image150);
        speed.add(R.drawable.image151);
        speed.add(R.drawable.image152);
        speed.add(R.drawable.image153);
        speed.add(R.drawable.image154);
        speed.add(R.drawable.image155);
        speed.add(R.drawable.image156);
        speed.add(R.drawable.image157);
        speed.add(R.drawable.image158);
        speed.add(R.drawable.image159);
        speed.add(R.drawable.image160);
        speed.add(R.drawable.image161);
        speed.add(R.drawable.image162);
        speed.add(R.drawable.image163);
        speed.add(R.drawable.image164);
        speed.add(R.drawable.image165);
        speed.add(R.drawable.image166);
        speed.add(R.drawable.image167);
        speed.add(R.drawable.image168);
        speed.add(R.drawable.image169);
        speed.add(R.drawable.image170);
        speed.add(R.drawable.image171);
        speed.add(R.drawable.image172);
        speed.add(R.drawable.image173);
        speed.add(R.drawable.image174);
        speed.add(R.drawable.image175);
        speed.add(R.drawable.image176);
        speed.add(R.drawable.image177);
        speed.add(R.drawable.image178);
        speed.add(R.drawable.image179);
        speed.add(R.drawable.image180);
        speed.add(R.drawable.image181);
        speed.add(R.drawable.image182);
        speed.add(R.drawable.image183);
        speed.add(R.drawable.image184);
        speed.add(R.drawable.image185);
        speed.add(R.drawable.image186);
        speed.add(R.drawable.image187);
        speed.add(R.drawable.image188);
        speed.add(R.drawable.image189);
        speed.add(R.drawable.image190);
        speed.add(R.drawable.image191);
        speed.add(R.drawable.image192);
        speed.add(R.drawable.image193);
        speed.add(R.drawable.image194);
        speed.add(R.drawable.image195);
        speed.add(R.drawable.image196);
        speed.add(R.drawable.image197);
        speed.add(R.drawable.image198);
        speed.add(R.drawable.image199);
        speed.add(R.drawable.image200);
        speed.add(R.drawable.image201);
        speed.add(R.drawable.image202);
        speed.add(R.drawable.image203);
        speed.add(R.drawable.image204);
        speed.add(R.drawable.image205);
        speed.add(R.drawable.image206);
        speed.add(R.drawable.image207);
        speed.add(R.drawable.image208);
        speed.add(R.drawable.image209);
        speed.add(R.drawable.image210);
        speed.add(R.drawable.image211);
        speed.add(R.drawable.image212);
        speed.add(R.drawable.image213);
        speed.add(R.drawable.image214);
        speed.add(R.drawable.image215);
        speed.add(R.drawable.image216);
        speed.add(R.drawable.image217);
        speed.add(R.drawable.image218);
        speed.add(R.drawable.image219);
        speed.add(R.drawable.image220);
        speed.add(R.drawable.image221);
        speed.add(R.drawable.image222);
        speed.add(R.drawable.image223);
        speed.add(R.drawable.image224);
        speed.add(R.drawable.image225);
        speed.add(R.drawable.image226);
        speed.add(R.drawable.image227);
        speed.add(R.drawable.image228);
        speed.add(R.drawable.image229);
        speed.add(R.drawable.image230);
        speed.add(R.drawable.image231);
        speed.add(R.drawable.image232);
        speed.add(R.drawable.image233);
        speed.add(R.drawable.image234);
        speed.add(R.drawable.image235);
        speed.add(R.drawable.image236);
        speed.add(R.drawable.image237);
        speed.add(R.drawable.image238);
        speed.add(R.drawable.image239);
        speed.add(R.drawable.image240);
        speed.add(R.drawable.image241);
        speed.add(R.drawable.image242);
        speed.add(R.drawable.image243);
        speed.add(R.drawable.image244);
        speed.add(R.drawable.image245);
        speed.add(R.drawable.image246);
        speed.add(R.drawable.image247);
        speed.add(R.drawable.image248);
        speed.add(R.drawable.image249);
        speed.add(R.drawable.image250);
        speed.add(R.drawable.image251);
        speed.add(R.drawable.image252);
        speed.add(R.drawable.image253);
        speed.add(R.drawable.image254);
        speed.add(R.drawable.image255);
        speed.add(R.drawable.image256);
        speed.add(R.drawable.image257);
        speed.add(R.drawable.image258);
        speed.add(R.drawable.image259);
        speed.add(R.drawable.image260);
        speed.add(R.drawable.image261);
        speed.add(R.drawable.image262);
        speed.add(R.drawable.image263);
        speed.add(R.drawable.image264);
        speed.add(R.drawable.image265);
        speed.add(R.drawable.image266);
        speed.add(R.drawable.image267);
        speed.add(R.drawable.image268);
        speed.add(R.drawable.image269);
        speed.add(R.drawable.image270);
        speed.add(R.drawable.image271);
        speed.add(R.drawable.image272);
        speed.add(R.drawable.image273);
        speed.add(R.drawable.image274);
        speed.add(R.drawable.image275);
        speed.add(R.drawable.image276);
        speed.add(R.drawable.image277);
        speed.add(R.drawable.image278);
        speed.add(R.drawable.image279);
        speed.add(R.drawable.image280);
        speed.add(R.drawable.image281);
        speed.add(R.drawable.image282);
        speed.add(R.drawable.image283);
        speed.add(R.drawable.image284);
        speed.add(R.drawable.image285);
        speed.add(R.drawable.image286);
        speed.add(R.drawable.image287);
        speed.add(R.drawable.image288);
        speed.add(R.drawable.image289);
        speed.add(R.drawable.image290);
        speed.add(R.drawable.image291);
        speed.add(R.drawable.image292);
        speed.add(R.drawable.image293);
        speed.add(R.drawable.image294);
        speed.add(R.drawable.image295);
        speed.add(R.drawable.image296);
        speed.add(R.drawable.image297);
        speed.add(R.drawable.image298);
        speed.add(R.drawable.image299);
        speed.add(R.drawable.image300);
        speed.add(R.drawable.image301);
        speed.add(R.drawable.image302);
        speed.add(R.drawable.image303);
        speed.add(R.drawable.image304);
        speed.add(R.drawable.image305);
        speed.add(R.drawable.image306);
        speed.add(R.drawable.image307);
        speed.add(R.drawable.image308);
        speed.add(R.drawable.image309);
        speed.add(R.drawable.image310);
        speed.add(R.drawable.image311);
        speed.add(R.drawable.image312);
        speed.add(R.drawable.image313);
        speed.add(R.drawable.image314);
        speed.add(R.drawable.image315);
        speed.add(R.drawable.image316);
        speed.add(R.drawable.image317);
        speed.add(R.drawable.image318);
        speed.add(R.drawable.image319);
        speed.add(R.drawable.image320);
        speed.add(R.drawable.image321);
        speed.add(R.drawable.image322);
        speed.add(R.drawable.image323);
        speed.add(R.drawable.image324);
        speed.add(R.drawable.image325);
        speed.add(R.drawable.image326);
        speed.add(R.drawable.image327);
        speed.add(R.drawable.image328);
        speed.add(R.drawable.image329);
        speed.add(R.drawable.image330);
        speed.add(R.drawable.image331);
        speed.add(R.drawable.image332);
        speed.add(R.drawable.image333);
        speed.add(R.drawable.image334);
        speed.add(R.drawable.image335);
        speed.add(R.drawable.image336);
        speed.add(R.drawable.image337);
        speed.add(R.drawable.image338);
        speed.add(R.drawable.image339);
        speed.add(R.drawable.image340);
        speed.add(R.drawable.image341);
        speed.add(R.drawable.image342);
        speed.add(R.drawable.image343);
        speed.add(R.drawable.image344);
        speed.add(R.drawable.image345);
        speed.add(R.drawable.image346);
        speed.add(R.drawable.image347);
        speed.add(R.drawable.image348);
        speed.add(R.drawable.image349);
        speed.add(R.drawable.image350);
        speed.add(R.drawable.image351);
        speed.add(R.drawable.image352);
        speed.add(R.drawable.image353);
        speed.add(R.drawable.image354);
        speed.add(R.drawable.image355);
        speed.add(R.drawable.image356);
        speed.add(R.drawable.image357);
        speed.add(R.drawable.image358);
        speed.add(R.drawable.image359);
        speed.add(R.drawable.image360);
        speed.add(R.drawable.image361);
        speed.add(R.drawable.image362);
        speed.add(R.drawable.image363);
        speed.add(R.drawable.image364);
        speed.add(R.drawable.image365);
        speed.add(R.drawable.image366);
        speed.add(R.drawable.image367);
        speed.add(R.drawable.image368);
        speed.add(R.drawable.image369);
        speed.add(R.drawable.image370);
        speed.add(R.drawable.image371);
        speed.add(R.drawable.image372);
        speed.add(R.drawable.image373);
        speed.add(R.drawable.image374);
        speed.add(R.drawable.image375);
        speed.add(R.drawable.image376);
        speed.add(R.drawable.image377);
        speed.add(R.drawable.image378);
        speed.add(R.drawable.image379);
        speed.add(R.drawable.image380);
        speed.add(R.drawable.image381);
        speed.add(R.drawable.image382);
        speed.add(R.drawable.image383);
        speed.add(R.drawable.image384);
        speed.add(R.drawable.image385);
        speed.add(R.drawable.image386);
        speed.add(R.drawable.image387);
        speed.add(R.drawable.image388);
        speed.add(R.drawable.image389);
        speed.add(R.drawable.image390);
        speed.add(R.drawable.image391);
        speed.add(R.drawable.image392);
        speed.add(R.drawable.image393);
        speed.add(R.drawable.image394);
        speed.add(R.drawable.image395);
        speed.add(R.drawable.image396);
        speed.add(R.drawable.image397);
        speed.add(R.drawable.image398);
        speed.add(R.drawable.image399);
        speed.add(R.drawable.image400);
        speed.add(R.drawable.image401);
        speed.add(R.drawable.image402);
        speed.add(R.drawable.image403);
        speed.add(R.drawable.image404);
        speed.add(R.drawable.image405);
        speed.add(R.drawable.image406);
        speed.add(R.drawable.image407);
        speed.add(R.drawable.image408);
        speed.add(R.drawable.image409);
        speed.add(R.drawable.image410);
        speed.add(R.drawable.image411);
        speed.add(R.drawable.image412);
        speed.add(R.drawable.image413);
        speed.add(R.drawable.image414);
        speed.add(R.drawable.image415);
        speed.add(R.drawable.image416);
        speed.add(R.drawable.image417);
        speed.add(R.drawable.image418);
        speed.add(R.drawable.image419);
        speed.add(R.drawable.image420);
        speed.add(R.drawable.image421);
        speed.add(R.drawable.image422);
        speed.add(R.drawable.image423);
        speed.add(R.drawable.image424);
        speed.add(R.drawable.image425);
        speed.add(R.drawable.image426);
        speed.add(R.drawable.image427);
        speed.add(R.drawable.image428);
        speed.add(R.drawable.image429);
        speed.add(R.drawable.image430);
        speed.add(R.drawable.image431);
        speed.add(R.drawable.image432);
        speed.add(R.drawable.image433);
        speed.add(R.drawable.image434);
        speed.add(R.drawable.image435);
        speed.add(R.drawable.image436);
        speed.add(R.drawable.image437);
        speed.add(R.drawable.image438);
        speed.add(R.drawable.image439);
        speed.add(R.drawable.image440);
        speed.add(R.drawable.image441);
        speed.add(R.drawable.image442);
        speed.add(R.drawable.image443);
        speed.add(R.drawable.image444);
        speed.add(R.drawable.image445);
        speed.add(R.drawable.image446);
        speed.add(R.drawable.image447);
        speed.add(R.drawable.image448);
        speed.add(R.drawable.image449);
        speed.add(R.drawable.image450);
        speed.add(R.drawable.image451);
        speed.add(R.drawable.image452);
        speed.add(R.drawable.image453);
        speed.add(R.drawable.image454);
        speed.add(R.drawable.image455);
        speed.add(R.drawable.image456);
        speed.add(R.drawable.image457);
        speed.add(R.drawable.image458);
        speed.add(R.drawable.image459);
        speed.add(R.drawable.image460);
        speed.add(R.drawable.image461);
        speed.add(R.drawable.image462);
        speed.add(R.drawable.image463);
        speed.add(R.drawable.image464);
        speed.add(R.drawable.image465);
        speed.add(R.drawable.image466);
        speed.add(R.drawable.image467);
        speed.add(R.drawable.image468);
        speed.add(R.drawable.image469);
        speed.add(R.drawable.image470);
        speed.add(R.drawable.image471);
        speed.add(R.drawable.image472);
        speed.add(R.drawable.image473);
        speed.add(R.drawable.image474);
        speed.add(R.drawable.image475);
        speed.add(R.drawable.image476);
        speed.add(R.drawable.image477);
        speed.add(R.drawable.image478);
        speed.add(R.drawable.image479);
        speed.add(R.drawable.image480);
        speed.add(R.drawable.image481);
        speed.add(R.drawable.image482);
        speed.add(R.drawable.image483);
        speed.add(R.drawable.image484);
        speed.add(R.drawable.image485);
        speed.add(R.drawable.image486);
        speed.add(R.drawable.image487);
        speed.add(R.drawable.image488);
        speed.add(R.drawable.image489);
        speed.add(R.drawable.image490);
        speed.add(R.drawable.image491);
        speed.add(R.drawable.image492);
        speed.add(R.drawable.image493);
        speed.add(R.drawable.image494);
        speed.add(R.drawable.image495);
        speed.add(R.drawable.image496);
        speed.add(R.drawable.image497);
        speed.add(R.drawable.image498);
        speed.add(R.drawable.image499);
        speed.add(R.drawable.image500);
        speed.add(R.drawable.image501);
        speed.add(R.drawable.image502);
        speed.add(R.drawable.image503);
        speed.add(R.drawable.image504);
        speed.add(R.drawable.image505);
        speed.add(R.drawable.image506);
        speed.add(R.drawable.image507);
        speed.add(R.drawable.image508);
        speed.add(R.drawable.image509);
        speed.add(R.drawable.image510);
        speed.add(R.drawable.image511);
        speed.add(R.drawable.image512);
        speed.add(R.drawable.image513);
        speed.add(R.drawable.image514);
        speed.add(R.drawable.image515);
        speed.add(R.drawable.image516);
        speed.add(R.drawable.image517);
        speed.add(R.drawable.image518);
        speed.add(R.drawable.image519);
        speed.add(R.drawable.image520);
        speed.add(R.drawable.image521);
        speed.add(R.drawable.image522);
        speed.add(R.drawable.image523);
        speed.add(R.drawable.image524);
        speed.add(R.drawable.image525);
        speed.add(R.drawable.image526);
        speed.add(R.drawable.image527);
        speed.add(R.drawable.image528);
        speed.add(R.drawable.image529);
        speed.add(R.drawable.image530);
        speed.add(R.drawable.image531);
        speed.add(R.drawable.image532);
        speed.add(R.drawable.image533);
        speed.add(R.drawable.image534);
        speed.add(R.drawable.image535);
        speed.add(R.drawable.image536);
        speed.add(R.drawable.image537);
        speed.add(R.drawable.image538);
        speed.add(R.drawable.image539);
        speed.add(R.drawable.image540);
        speed.add(R.drawable.image541);
        speed.add(R.drawable.image542);
        speed.add(R.drawable.image543);
        speed.add(R.drawable.image544);
        speed.add(R.drawable.image545);
        speed.add(R.drawable.image546);
        speed.add(R.drawable.image547);
        speed.add(R.drawable.image548);
        speed.add(R.drawable.image549);
        speed.add(R.drawable.image550);
        speed.add(R.drawable.image551);
        speed.add(R.drawable.image552);
        speed.add(R.drawable.image553);
        speed.add(R.drawable.image554);
        speed.add(R.drawable.image555);
        speed.add(R.drawable.image556);
        speed.add(R.drawable.image557);
        speed.add(R.drawable.image558);
        speed.add(R.drawable.image559);
        speed.add(R.drawable.image560);
        speed.add(R.drawable.image561);
        speed.add(R.drawable.image562);
        speed.add(R.drawable.image563);
        speed.add(R.drawable.image564);
        speed.add(R.drawable.image565);
        speed.add(R.drawable.image566);
        speed.add(R.drawable.image567);
        speed.add(R.drawable.image568);
        speed.add(R.drawable.image569);
        speed.add(R.drawable.image570);
        speed.add(R.drawable.image571);
        speed.add(R.drawable.image572);
        speed.add(R.drawable.image573);
        speed.add(R.drawable.image574);
        speed.add(R.drawable.image575);
        speed.add(R.drawable.image576);
        speed.add(R.drawable.image577);
        speed.add(R.drawable.image578);
        speed.add(R.drawable.image579);
        speed.add(R.drawable.image580);
        speed.add(R.drawable.image581);
        speed.add(R.drawable.image582);
        speed.add(R.drawable.image583);
        speed.add(R.drawable.image584);
        speed.add(R.drawable.image585);
        speed.add(R.drawable.image586);
        speed.add(R.drawable.image587);
        speed.add(R.drawable.image588);
        speed.add(R.drawable.image589);
        speed.add(R.drawable.image590);
        speed.add(R.drawable.image591);
        speed.add(R.drawable.image592);
        speed.add(R.drawable.image593);
        speed.add(R.drawable.image594);
        speed.add(R.drawable.image595);
        speed.add(R.drawable.image596);
        speed.add(R.drawable.image597);
        speed.add(R.drawable.image598);
        speed.add(R.drawable.image599);
        speed.add(R.drawable.image600);
        speed.add(R.drawable.image601);
        speed.add(R.drawable.image602);
        speed.add(R.drawable.image603);
        speed.add(R.drawable.image604);
        speed.add(R.drawable.image605);
        speed.add(R.drawable.image606);
        speed.add(R.drawable.image607);
        speed.add(R.drawable.image608);
        speed.add(R.drawable.image609);
        speed.add(R.drawable.image610);
        speed.add(R.drawable.image611);
        speed.add(R.drawable.image612);
        speed.add(R.drawable.image613);
        speed.add(R.drawable.image614);
        speed.add(R.drawable.image615);
        speed.add(R.drawable.image616);
        speed.add(R.drawable.image617);
        speed.add(R.drawable.image618);
        speed.add(R.drawable.image619);
        speed.add(R.drawable.image620);
        speed.add(R.drawable.image621);
        speed.add(R.drawable.image622);
        speed.add(R.drawable.image623);
        speed.add(R.drawable.image624);
        speed.add(R.drawable.image625);
        speed.add(R.drawable.image626);
        speed.add(R.drawable.image627);
        speed.add(R.drawable.image628);
        speed.add(R.drawable.image629);
        speed.add(R.drawable.image630);
        speed.add(R.drawable.image631);
        speed.add(R.drawable.image632);
        speed.add(R.drawable.image633);
        speed.add(R.drawable.image634);
        speed.add(R.drawable.image635);
        speed.add(R.drawable.image636);
        speed.add(R.drawable.image637);
        speed.add(R.drawable.image638);
        speed.add(R.drawable.image639);
        speed.add(R.drawable.image640);
        speed.add(R.drawable.image641);
        speed.add(R.drawable.image642);
        speed.add(R.drawable.image643);
        speed.add(R.drawable.image644);
        speed.add(R.drawable.image645);
        speed.add(R.drawable.image646);
        speed.add(R.drawable.image647);
        speed.add(R.drawable.image648);
        speed.add(R.drawable.image649);
        speed.add(R.drawable.image650);
        speed.add(R.drawable.image651);
        speed.add(R.drawable.image652);
        speed.add(R.drawable.image653);
        speed.add(R.drawable.image654);
        speed.add(R.drawable.image655);
        speed.add(R.drawable.image656);
        speed.add(R.drawable.image657);
        speed.add(R.drawable.image658);
        speed.add(R.drawable.image659);
        speed.add(R.drawable.image660);
        speed.add(R.drawable.image661);
        speed.add(R.drawable.image662);
        speed.add(R.drawable.image663);
        speed.add(R.drawable.image664);
        speed.add(R.drawable.image665);
        speed.add(R.drawable.image666);
        speed.add(R.drawable.image667);
        speed.add(R.drawable.image668);
        speed.add(R.drawable.image669);
        speed.add(R.drawable.image670);
        speed.add(R.drawable.image671);
        speed.add(R.drawable.image672);
        speed.add(R.drawable.image673);
        speed.add(R.drawable.image674);
        speed.add(R.drawable.image675);
        speed.add(R.drawable.image676);
        speed.add(R.drawable.image677);
        speed.add(R.drawable.image678);
        speed.add(R.drawable.image679);
        speed.add(R.drawable.image680);
        speed.add(R.drawable.image681);
        speed.add(R.drawable.image682);
        speed.add(R.drawable.image683);
        speed.add(R.drawable.image684);
        speed.add(R.drawable.image685);
        speed.add(R.drawable.image686);
        speed.add(R.drawable.image687);
        speed.add(R.drawable.image688);
        speed.add(R.drawable.image689);
        speed.add(R.drawable.image690);
        speed.add(R.drawable.image691);
        speed.add(R.drawable.image692);
        speed.add(R.drawable.image693);
        speed.add(R.drawable.image694);
        speed.add(R.drawable.image695);
        speed.add(R.drawable.image696);
        speed.add(R.drawable.image697);
        speed.add(R.drawable.image698);
        speed.add(R.drawable.image699);
        speed.add(R.drawable.image700);
        speed.add(R.drawable.image701);
        speed.add(R.drawable.image702);
        speed.add(R.drawable.image703);
        speed.add(R.drawable.image704);
        speed.add(R.drawable.image705);
        speed.add(R.drawable.image706);
        speed.add(R.drawable.image707);
        speed.add(R.drawable.image708);
        speed.add(R.drawable.image709);
        speed.add(R.drawable.image710);
        speed.add(R.drawable.image711);
        speed.add(R.drawable.image712);
        speed.add(R.drawable.image713);
        speed.add(R.drawable.image714);
        speed.add(R.drawable.image715);
        speed.add(R.drawable.image716);
        speed.add(R.drawable.image717);
        speed.add(R.drawable.image718);
        speed.add(R.drawable.image719);
        speed.add(R.drawable.image720);
        speed.add(R.drawable.image721);
        speed.add(R.drawable.image722);
        speed.add(R.drawable.image723);
        speed.add(R.drawable.image724);
        speed.add(R.drawable.image725);
        speed.add(R.drawable.image726);
        speed.add(R.drawable.image727);
        speed.add(R.drawable.image728);
        speed.add(R.drawable.image729);
        speed.add(R.drawable.image730);
        speed.add(R.drawable.image731);
        speed.add(R.drawable.image732);
        speed.add(R.drawable.image733);
        speed.add(R.drawable.image734);
        speed.add(R.drawable.image735);
        speed.add(R.drawable.image736);
        speed.add(R.drawable.image737);
        speed.add(R.drawable.image738);
        speed.add(R.drawable.image739);
        speed.add(R.drawable.image740);
        speed.add(R.drawable.image741);
        speed.add(R.drawable.image742);
        speed.add(R.drawable.image743);
        speed.add(R.drawable.image744);
        speed.add(R.drawable.image745);
        speed.add(R.drawable.image746);
        speed.add(R.drawable.image747);
        speed.add(R.drawable.image748);
        speed.add(R.drawable.image749);
        speed.add(R.drawable.image750);
        speed.add(R.drawable.image751);
        speed.add(R.drawable.image752);
        speed.add(R.drawable.image753);
        speed.add(R.drawable.image754);
        speed.add(R.drawable.image755);
        speed.add(R.drawable.image756);
        speed.add(R.drawable.image757);
        speed.add(R.drawable.image758);
        speed.add(R.drawable.image759);
        speed.add(R.drawable.image760);
        speed.add(R.drawable.image761);
        speed.add(R.drawable.image762);
        speed.add(R.drawable.image763);
        speed.add(R.drawable.image764);
        speed.add(R.drawable.image765);
        speed.add(R.drawable.image766);
        speed.add(R.drawable.image767);
        speed.add(R.drawable.image768);
        speed.add(R.drawable.image769);
        speed.add(R.drawable.image770);
        speed.add(R.drawable.image771);
        speed.add(R.drawable.image772);
        speed.add(R.drawable.image773);
        speed.add(R.drawable.image774);
        speed.add(R.drawable.image775);
        speed.add(R.drawable.image776);
        speed.add(R.drawable.image777);
        speed.add(R.drawable.image778);
        speed.add(R.drawable.image779);
        speed.add(R.drawable.image780);
        speed.add(R.drawable.image781);
        speed.add(R.drawable.image782);
        speed.add(R.drawable.image783);
        speed.add(R.drawable.image784);
        speed.add(R.drawable.image785);
        speed.add(R.drawable.image786);
        speed.add(R.drawable.image787);
        speed.add(R.drawable.image788);
        speed.add(R.drawable.image789);
        speed.add(R.drawable.image790);
        speed.add(R.drawable.image791);
        speed.add(R.drawable.image792);
        speed.add(R.drawable.image793);
        speed.add(R.drawable.image794);
        speed.add(R.drawable.image795);
        speed.add(R.drawable.image796);
        speed.add(R.drawable.image797);
        speed.add(R.drawable.image798);
        speed.add(R.drawable.image799);
        speed.add(R.drawable.image800);
        speed.add(R.drawable.image801);
        speed.add(R.drawable.image802);
        speed.add(R.drawable.image803);
        speed.add(R.drawable.image804);
        speed.add(R.drawable.image805);
        speed.add(R.drawable.image806);
        speed.add(R.drawable.image807);
        speed.add(R.drawable.image808);
        speed.add(R.drawable.image809);
        speed.add(R.drawable.image810);
        speed.add(R.drawable.image811);
        speed.add(R.drawable.image812);
        speed.add(R.drawable.image813);
        speed.add(R.drawable.image814);
        speed.add(R.drawable.image815);
        speed.add(R.drawable.image816);
        speed.add(R.drawable.image817);
        speed.add(R.drawable.image818);
        speed.add(R.drawable.image819);
        speed.add(R.drawable.image820);
        speed.add(R.drawable.image821);
        speed.add(R.drawable.image822);
        speed.add(R.drawable.image823);
        speed.add(R.drawable.image824);
        speed.add(R.drawable.image825);
        speed.add(R.drawable.image826);
        speed.add(R.drawable.image827);
        speed.add(R.drawable.image828);
        speed.add(R.drawable.image829);
        speed.add(R.drawable.image830);
        speed.add(R.drawable.image831);
        speed.add(R.drawable.image832);
        speed.add(R.drawable.image833);
        speed.add(R.drawable.image834);
        speed.add(R.drawable.image835);
        speed.add(R.drawable.image836);
        speed.add(R.drawable.image837);
        speed.add(R.drawable.image838);
        speed.add(R.drawable.image839);
        speed.add(R.drawable.image840);
        speed.add(R.drawable.image841);
        speed.add(R.drawable.image842);
        speed.add(R.drawable.image843);
        speed.add(R.drawable.image844);
        speed.add(R.drawable.image845);
        speed.add(R.drawable.image846);
        speed.add(R.drawable.image847);
        speed.add(R.drawable.image848);
        speed.add(R.drawable.image849);
        speed.add(R.drawable.image850);
        speed.add(R.drawable.image851);
        speed.add(R.drawable.image852);
        speed.add(R.drawable.image853);
        speed.add(R.drawable.image854);
        speed.add(R.drawable.image855);
        speed.add(R.drawable.image856);
        speed.add(R.drawable.image857);
        speed.add(R.drawable.image858);
        speed.add(R.drawable.image859);
        speed.add(R.drawable.image860);
        speed.add(R.drawable.image861);
        speed.add(R.drawable.image862);
        speed.add(R.drawable.image863);
        speed.add(R.drawable.image864);
        speed.add(R.drawable.image865);
        speed.add(R.drawable.image866);
        speed.add(R.drawable.image867);
        speed.add(R.drawable.image868);
        speed.add(R.drawable.image869);
        speed.add(R.drawable.image870);
        speed.add(R.drawable.image871);
        speed.add(R.drawable.image872);
        speed.add(R.drawable.image873);
        speed.add(R.drawable.image874);
        speed.add(R.drawable.image875);
        speed.add(R.drawable.image876);
        speed.add(R.drawable.image877);
        speed.add(R.drawable.image878);
        speed.add(R.drawable.image879);
        speed.add(R.drawable.image880);
        speed.add(R.drawable.image881);
        speed.add(R.drawable.image882);
        speed.add(R.drawable.image883);
        speed.add(R.drawable.image884);
        speed.add(R.drawable.image885);
        speed.add(R.drawable.image886);
        speed.add(R.drawable.image887);
        speed.add(R.drawable.image888);
        speed.add(R.drawable.image889);
        speed.add(R.drawable.image890);
        speed.add(R.drawable.image891);
        speed.add(R.drawable.image892);
        speed.add(R.drawable.image893);
        speed.add(R.drawable.image894);
        speed.add(R.drawable.image895);
        speed.add(R.drawable.image896);
        speed.add(R.drawable.image897);
        speed.add(R.drawable.image898);
        speed.add(R.drawable.image899);
        speed.add(R.drawable.image900);
        speed.add(R.drawable.image901);
        speed.add(R.drawable.image902);
        speed.add(R.drawable.image903);
        speed.add(R.drawable.image904);
        speed.add(R.drawable.image905);
        speed.add(R.drawable.image906);
        speed.add(R.drawable.image907);
        speed.add(R.drawable.image908);
        speed.add(R.drawable.image909);
        speed.add(R.drawable.image910);
        speed.add(R.drawable.image911);
        speed.add(R.drawable.image912);
        speed.add(R.drawable.image913);
        speed.add(R.drawable.image914);
        speed.add(R.drawable.image915);
        speed.add(R.drawable.image916);
        speed.add(R.drawable.image917);
        speed.add(R.drawable.image918);
        speed.add(R.drawable.image919);
        speed.add(R.drawable.image920);
        speed.add(R.drawable.image921);
        speed.add(R.drawable.image922);
        speed.add(R.drawable.image923);
        speed.add(R.drawable.image924);
        speed.add(R.drawable.image925);
        speed.add(R.drawable.image926);
        speed.add(R.drawable.image927);
        speed.add(R.drawable.image928);
        speed.add(R.drawable.image929);
        speed.add(R.drawable.image930);
        speed.add(R.drawable.image931);
        speed.add(R.drawable.image932);
        speed.add(R.drawable.image933);
        speed.add(R.drawable.image934);
        speed.add(R.drawable.image935);
        speed.add(R.drawable.image936);
        speed.add(R.drawable.image937);
        speed.add(R.drawable.image938);
        speed.add(R.drawable.image939);
        speed.add(R.drawable.image940);
        speed.add(R.drawable.image941);
        speed.add(R.drawable.image942);
        speed.add(R.drawable.image943);
        speed.add(R.drawable.image944);
        speed.add(R.drawable.image945);
        speed.add(R.drawable.image946);
        speed.add(R.drawable.image947);
        speed.add(R.drawable.image948);
        speed.add(R.drawable.image949);
        speed.add(R.drawable.image950);
        speed.add(R.drawable.image951);
        speed.add(R.drawable.image952);
        speed.add(R.drawable.image953);
        speed.add(R.drawable.image954);
        speed.add(R.drawable.image955);
        speed.add(R.drawable.image956);
        speed.add(R.drawable.image957);
        speed.add(R.drawable.image958);
        speed.add(R.drawable.image959);
        speed.add(R.drawable.image960);
        speed.add(R.drawable.image961);
        speed.add(R.drawable.image962);
        speed.add(R.drawable.image963);
        speed.add(R.drawable.image964);
        speed.add(R.drawable.image965);
        speed.add(R.drawable.image966);
        speed.add(R.drawable.image967);
        speed.add(R.drawable.image968);
        speed.add(R.drawable.image969);
        speed.add(R.drawable.image970);
        speed.add(R.drawable.image971);
        speed.add(R.drawable.image972);
        speed.add(R.drawable.image973);
        speed.add(R.drawable.image974);
        speed.add(R.drawable.image975);
        speed.add(R.drawable.image976);
        speed.add(R.drawable.image977);
        speed.add(R.drawable.image978);
        speed.add(R.drawable.image979);
        speed.add(R.drawable.image980);
        speed.add(R.drawable.image981);
        speed.add(R.drawable.image982);
        speed.add(R.drawable.image983);
        speed.add(R.drawable.image984);
        speed.add(R.drawable.image985);
        speed.add(R.drawable.image986);
        speed.add(R.drawable.image987);
        speed.add(R.drawable.image988);
        speed.add(R.drawable.image989);
        speed.add(R.drawable.image990);
        speed.add(R.drawable.image991);
        speed.add(R.drawable.image992);
        speed.add(R.drawable.image993);
        speed.add(R.drawable.image994);
        speed.add(R.drawable.image995);
        speed.add(R.drawable.image996);
        speed.add(R.drawable.image997);
        speed.add(R.drawable.image998);
        speed.add(R.drawable.image999);

        speed.add(R.drawable.imagemb010);
        speed.add(R.drawable.imagemb011);
        speed.add(R.drawable.imagemb012);
        speed.add(R.drawable.imagemb013);
        speed.add(R.drawable.imagemb014);
        speed.add(R.drawable.imagemb015);
        speed.add(R.drawable.imagemb016);
        speed.add(R.drawable.imagemb017);
        speed.add(R.drawable.imagemb018);
        speed.add(R.drawable.imagemb019);
        speed.add(R.drawable.imagemb020);
        speed.add(R.drawable.imagemb021);
        speed.add(R.drawable.imagemb022);
        speed.add(R.drawable.imagemb023);
        speed.add(R.drawable.imagemb024);
        speed.add(R.drawable.imagemb025);
        speed.add(R.drawable.imagemb026);
        speed.add(R.drawable.imagemb027);
        speed.add(R.drawable.imagemb028);
        speed.add(R.drawable.imagemb029);
        speed.add(R.drawable.imagemb030);
        speed.add(R.drawable.imagemb031);
        speed.add(R.drawable.imagemb032);
        speed.add(R.drawable.imagemb033);
        speed.add(R.drawable.imagemb034);
        speed.add(R.drawable.imagemb035);
        speed.add(R.drawable.imagemb036);
        speed.add(R.drawable.imagemb037);
        speed.add(R.drawable.imagemb038);
        speed.add(R.drawable.imagemb039);
        speed.add(R.drawable.imagemb040);
        speed.add(R.drawable.imagemb041);
        speed.add(R.drawable.imagemb042);
        speed.add(R.drawable.imagemb043);
        speed.add(R.drawable.imagemb044);
        speed.add(R.drawable.imagemb045);
        speed.add(R.drawable.imagemb046);
        speed.add(R.drawable.imagemb047);
        speed.add(R.drawable.imagemb048);
        speed.add(R.drawable.imagemb049);
        speed.add(R.drawable.imagemb050);
        speed.add(R.drawable.imagemb051);
        speed.add(R.drawable.imagemb052);
        speed.add(R.drawable.imagemb053);
        speed.add(R.drawable.imagemb054);
        speed.add(R.drawable.imagemb055);
        speed.add(R.drawable.imagemb056);
        speed.add(R.drawable.imagemb057);
        speed.add(R.drawable.imagemb058);
        speed.add(R.drawable.imagemb059);
        speed.add(R.drawable.imagemb060);
        speed.add(R.drawable.imagemb061);
        speed.add(R.drawable.imagemb062);
        speed.add(R.drawable.imagemb063);
        speed.add(R.drawable.imagemb064);
        speed.add(R.drawable.imagemb065);
        speed.add(R.drawable.imagemb066);
        speed.add(R.drawable.imagemb067);
        speed.add(R.drawable.imagemb068);
        speed.add(R.drawable.imagemb069);
        speed.add(R.drawable.imagemb070);
        speed.add(R.drawable.imagemb071);
        speed.add(R.drawable.imagemb072);
        speed.add(R.drawable.imagemb073);
        speed.add(R.drawable.imagemb074);
        speed.add(R.drawable.imagemb075);
        speed.add(R.drawable.imagemb076);
        speed.add(R.drawable.imagemb077);
        speed.add(R.drawable.imagemb078);
        speed.add(R.drawable.imagemb079);
        speed.add(R.drawable.imagemb080);
        speed.add(R.drawable.imagemb081);
        speed.add(R.drawable.imagemb082);
        speed.add(R.drawable.imagemb083);
        speed.add(R.drawable.imagemb084);
        speed.add(R.drawable.imagemb085);
        speed.add(R.drawable.imagemb086);
        speed.add(R.drawable.imagemb087);
        speed.add(R.drawable.imagemb088);
        speed.add(R.drawable.imagemb089);
        speed.add(R.drawable.imagemb090);
        speed.add(R.drawable.imagemb091);
        speed.add(R.drawable.imagemb092);
        speed.add(R.drawable.imagemb093);
        speed.add(R.drawable.imagemb094);
        speed.add(R.drawable.imagemb095);
        speed.add(R.drawable.imagemb096);
        speed.add(R.drawable.imagemb097);
        speed.add(R.drawable.imagemb098);
        speed.add(R.drawable.imagemb099);
        speed.add(R.drawable.imagemb100);
        speed.add(R.drawable.imagemb101);
        speed.add(R.drawable.imagemb102);
        speed.add(R.drawable.imagemb103);
        speed.add(R.drawable.imagemb104);
        speed.add(R.drawable.imagemb105);
        speed.add(R.drawable.imagemb106);
        speed.add(R.drawable.imagemb107);
        speed.add(R.drawable.imagemb108);
        speed.add(R.drawable.imagemb109);
        speed.add(R.drawable.imagemb110);
        speed.add(R.drawable.imagemb111);
        speed.add(R.drawable.imagemb112);
        speed.add(R.drawable.imagemb113);
        speed.add(R.drawable.imagemb114);
        speed.add(R.drawable.imagemb115);
        speed.add(R.drawable.imagemb116);
        speed.add(R.drawable.imagemb117);
        speed.add(R.drawable.imagemb118);
        speed.add(R.drawable.imagemb119);
        speed.add(R.drawable.imagemb120);
        speed.add(R.drawable.imagemb121);
        speed.add(R.drawable.imagemb122);
        speed.add(R.drawable.imagemb123);
        speed.add(R.drawable.imagemb124);
        speed.add(R.drawable.imagemb125);
        speed.add(R.drawable.imagemb126);
        speed.add(R.drawable.imagemb127);
        speed.add(R.drawable.imagemb128);
        speed.add(R.drawable.imagemb129);
        speed.add(R.drawable.imagemb130);
        speed.add(R.drawable.imagemb131);
        speed.add(R.drawable.imagemb132);
        speed.add(R.drawable.imagemb133);
        speed.add(R.drawable.imagemb134);
        speed.add(R.drawable.imagemb135);
        speed.add(R.drawable.imagemb136);
        speed.add(R.drawable.imagemb137);
        speed.add(R.drawable.imagemb138);
        speed.add(R.drawable.imagemb139);
        speed.add(R.drawable.imagemb140);
        speed.add(R.drawable.imagemb141);
        speed.add(R.drawable.imagemb142);
        speed.add(R.drawable.imagemb143);
        speed.add(R.drawable.imagemb144);
        speed.add(R.drawable.imagemb145);
        speed.add(R.drawable.imagemb146);
        speed.add(R.drawable.imagemb147);
        speed.add(R.drawable.imagemb148);
        speed.add(R.drawable.imagemb149);
        speed.add(R.drawable.imagemb150);
        speed.add(R.drawable.imagemb151);
        speed.add(R.drawable.imagemb152);
        speed.add(R.drawable.imagemb153);
        speed.add(R.drawable.imagemb154);
        speed.add(R.drawable.imagemb155);
        speed.add(R.drawable.imagemb156);
        speed.add(R.drawable.imagemb157);
        speed.add(R.drawable.imagemb158);
        speed.add(R.drawable.imagemb159);
        speed.add(R.drawable.imagemb160);
        speed.add(R.drawable.imagemb161);
        speed.add(R.drawable.imagemb162);
        speed.add(R.drawable.imagemb163);
        speed.add(R.drawable.imagemb164);
        speed.add(R.drawable.imagemb165);
        speed.add(R.drawable.imagemb166);
        speed.add(R.drawable.imagemb167);
        speed.add(R.drawable.imagemb168);
        speed.add(R.drawable.imagemb169);
        speed.add(R.drawable.imagemb170);
        speed.add(R.drawable.imagemb171);
        speed.add(R.drawable.imagemb172);
        speed.add(R.drawable.imagemb173);
        speed.add(R.drawable.imagemb174);
        speed.add(R.drawable.imagemb175);
        speed.add(R.drawable.imagemb176);
        speed.add(R.drawable.imagemb177);
        speed.add(R.drawable.imagemb178);
        speed.add(R.drawable.imagemb179);
        speed.add(R.drawable.imagemb180);
        speed.add(R.drawable.imagemb181);
        speed.add(R.drawable.imagemb182);
        speed.add(R.drawable.imagemb183);
        speed.add(R.drawable.imagemb184);
        speed.add(R.drawable.imagemb185);
        speed.add(R.drawable.imagemb186);
        speed.add(R.drawable.imagemb187);
        speed.add(R.drawable.imagemb188);
        speed.add(R.drawable.imagemb189);
        speed.add(R.drawable.imagemb190);
        speed.add(R.drawable.imagemb191);
        speed.add(R.drawable.imagemb192);
        speed.add(R.drawable.imagemb193);
        speed.add(R.drawable.imagemb194);
        speed.add(R.drawable.imagemb195);
        speed.add(R.drawable.imagemb196);
        speed.add(R.drawable.imagemb197);
        speed.add(R.drawable.imagemb198);
        speed.add(R.drawable.imagemb199);
        speed.add(R.drawable.imagemb200);
        speed.add(R.drawable.imagemb201);
        speed.add(R.drawable.imagemb202);
        speed.add(R.drawable.imagemb203);
        speed.add(R.drawable.imagemb204);
        speed.add(R.drawable.imagemb205);
        speed.add(R.drawable.imagemb206);
        speed.add(R.drawable.imagemb207);
        speed.add(R.drawable.imagemb208);
        speed.add(R.drawable.imagemb209);
        speed.add(R.drawable.imagemb210);
        speed.add(R.drawable.imagemb211);
        speed.add(R.drawable.imagemb212);
        speed.add(R.drawable.imagemb213);
        speed.add(R.drawable.imagemb214);
        speed.add(R.drawable.imagemb215);
        speed.add(R.drawable.imagemb216);
        speed.add(R.drawable.imagemb217);
        speed.add(R.drawable.imagemb218);
        speed.add(R.drawable.imagemb219);
        speed.add(R.drawable.imagemb220);
        speed.add(R.drawable.imagemb221);
        speed.add(R.drawable.imagemb222);
        speed.add(R.drawable.imagemb223);
        speed.add(R.drawable.imagemb224);
        speed.add(R.drawable.imagemb225);
        speed.add(R.drawable.imagemb226);
        speed.add(R.drawable.imagemb227);
        speed.add(R.drawable.imagemb228);
        speed.add(R.drawable.imagemb229);
        speed.add(R.drawable.imagemb230);
        speed.add(R.drawable.imagemb231);
        speed.add(R.drawable.imagemb232);
        speed.add(R.drawable.imagemb233);
        speed.add(R.drawable.imagemb234);
        speed.add(R.drawable.imagemb235);
        speed.add(R.drawable.imagemb236);
        speed.add(R.drawable.imagemb237);
        speed.add(R.drawable.imagemb238);
        speed.add(R.drawable.imagemb239);
        speed.add(R.drawable.imagemb240);
        speed.add(R.drawable.imagemb241);
        speed.add(R.drawable.imagemb242);
        speed.add(R.drawable.imagemb243);
        speed.add(R.drawable.imagemb244);
        speed.add(R.drawable.imagemb245);
        speed.add(R.drawable.imagemb246);
        speed.add(R.drawable.imagemb247);
        speed.add(R.drawable.imagemb248);
        speed.add(R.drawable.imagemb249);
        speed.add(R.drawable.imagemb250);
        speed.add(R.drawable.imagemb251);
        speed.add(R.drawable.imagemb252);
        speed.add(R.drawable.imagemb253);
        speed.add(R.drawable.imagemb254);
        speed.add(R.drawable.imagemb255);
        speed.add(R.drawable.imagemb256);
        speed.add(R.drawable.imagemb257);
        speed.add(R.drawable.imagemb258);
        speed.add(R.drawable.imagemb259);
        speed.add(R.drawable.imagemb260);
        speed.add(R.drawable.imagemb261);
        speed.add(R.drawable.imagemb262);
        speed.add(R.drawable.imagemb263);
        speed.add(R.drawable.imagemb264);
        speed.add(R.drawable.imagemb265);
        speed.add(R.drawable.imagemb266);
        speed.add(R.drawable.imagemb267);
        speed.add(R.drawable.imagemb268);
        speed.add(R.drawable.imagemb269);
        speed.add(R.drawable.imagemb270);
        speed.add(R.drawable.imagemb271);
        speed.add(R.drawable.imagemb272);
        speed.add(R.drawable.imagemb273);
        speed.add(R.drawable.imagemb274);
        speed.add(R.drawable.imagemb275);
        speed.add(R.drawable.imagemb276);
        speed.add(R.drawable.imagemb277);
        speed.add(R.drawable.imagemb278);
        speed.add(R.drawable.imagemb279);
        speed.add(R.drawable.imagemb280);
        speed.add(R.drawable.imagemb281);
        speed.add(R.drawable.imagemb282);
        speed.add(R.drawable.imagemb283);
        speed.add(R.drawable.imagemb284);
        speed.add(R.drawable.imagemb285);
        speed.add(R.drawable.imagemb286);
        speed.add(R.drawable.imagemb287);
        speed.add(R.drawable.imagemb288);
        speed.add(R.drawable.imagemb289);
        speed.add(R.drawable.imagemb290);
        speed.add(R.drawable.imagemb291);

    }

    private void startInBackground() {
        Log.e("First", "First");
        Intent myIntent = new Intent(this, Landing_Page.class);
        pendingIntent = PendingIntent.getActivity(this, 0, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.image000)
                .setContentTitle("")
                .setContentText("")
                .setContentIntent(pendingIntent);
        handler = new Handler();
        updateTask = new Runnable() {
            @Override
            public void run() {
                update();
                handler.postDelayed(this, 1000);
            }
        };
        if (!handlerCheck) {
            handler.postDelayed(updateTask, 0);
            handlerCheck = true;
        }
        startForeground(1, builder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        connMgr = (ConnectivityManager)
                getSystemService(CONNECTIVITY_SERVICE);

        // check for wifi
        wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        // check for mobile data
        wifiMgr = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        return START_STICKY;
    }

    void update() {

        if(firstTime == true){
            if (b) {
                up1 = TrafficStats.getTotalTxBytes();
                dn1 = TrafficStats.getTotalRxBytes();
                b = false;
            } else {
                up2 = TrafficStats.getTotalTxBytes();
                dn2 = TrafficStats.getTotalRxBytes();
                b = true;
            }
            String s = "" + (Math.abs(up1 - up2) / 1000);
            String s2 = "" + (Math.abs(dn1 - dn2) / 1000);
            //Log.e("upload", s);
            //Log.e("download", s2);
            //mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            //rv.setTextViewText(R.id.textView5, "Upload: " + s + " KB/s");
            //rv.setTextViewText(R.id.textView4, "Download: " + s2 + "KB/s");
            String g;
            g = isInternetConnected(Background.this);

            if (g.equals("wifi")) {
                wifiInfo = wifiMgr.getConnectionInfo();
                name = wifiInfo.getSSID();
                totalDownWifi = sp.getLong("totalDownWifi", 0);
                totalUpWifi = sp.getLong("totalUpWifi", 0);
                totalDownWifi1 = sp.getLong("totalDownWifi1", 0);
                totalUpWifi1 = sp.getLong("totalUpWifi1", 0);
                totalWifi = sp.getLong("totalWifi", 0);
                current = sp.getLong("current_wifi", 0);

                totalDownWifi += (Math.abs(dn1 - dn2) / 1000);
                totalUpWifi += (Math.abs(up1 - up2) / 1000);
                totalDownWifi1 += (Math.abs(dn1 - dn2) / 1000);
                totalUpWifi1 += (Math.abs(up1 - up2) / 1000);


                current = (Math.abs(dn1 - dn2) / 1000) + (Math.abs(up1 - up2) / 1000);

                editor.putLong("totalDownWifi", totalDownWifi);
                editor.putLong("totalDownWifi1", totalDownWifi1);
                editor.putLong("totalWifi", (totalDownWifi  + totalUpWifi) / 1000);
                //Log.e("Total Wifi", "" + sp.getLong("totalWifi", 0));
                editor.putLong("totalUpWifi", totalUpWifi);
                editor.putLong("totalUpWifi1", totalUpWifi1);
                editor.putLong("current_wifi", current);
                editor.commit();

            } else if (g.equals("mobile")) {
                totalDownMob = sp.getLong("totalDownMob", 0);
                totalUpMob = sp.getLong("totalUpMob", 0);
                totalDownMob1 = sp.getLong("totalDownMob1", 0);
                totalUpMob1 = sp.getLong("totalUpMob1", 0);
                current = sp.getLong("current_mobile", 0);
                totalMob = sp.getLong("totalMob", 0);

                totalDownMob += (Math.abs(dn1 - dn2) / 1000);
                totalUpMob += (Math.abs(up1 - up2) / 1000);
                totalDownMob1 += (Math.abs(dn1 - dn2) / 1000);
                totalUpMob1 += (Math.abs(up1 - up2) / 1000);

                current = (Math.abs(dn1 - dn2) / 1000) + (Math.abs(up1 - up2) / 1000);

                editor.putLong("totalDownMob", totalDownMob);
                editor.putLong("totalDownMob1", totalDownMob1);
                editor.putLong("totalMob", (totalDownMob  + totalUpMob) / 1024);
                editor.putLong("totalUpMob", totalUpMob);
                editor.putLong("totalUpMob1", totalUpMob1);
                editor.putLong("current_mobile", current);
                editor.commit();
            } else {
                //rv.setTextViewText(R.id.textView2, "No Network");
            }

            int t = Integer.parseInt(s) + Integer.parseInt(s2);

            if (t > 1000) {
                t = ((t % 1000) / 100) + 999;
            }
            if(t > speed.size()){
                t = speed.size() - 1;
            }
            nm.notify(1, builder.setContentTitle("up: " + s + " down: " + s2)
                    .setContentText(g)
                    .setSmallIcon(speed.get(t))
                    .setContentIntent(contentIntent)
                    .setPriority(128)
                    .build());
            //Log.e("testing", "" + t);
            freeMemory();
            //awm.updateAppWidget(thisWidget, rv);
        }else{
            firstTime = true;
            if (b) {
                up1 = TrafficStats.getTotalTxBytes();
                dn1 = TrafficStats.getTotalRxBytes();
                up2 = TrafficStats.getTotalTxBytes();
                dn2 = TrafficStats.getTotalRxBytes();
                b = false;
            } else {
                up1 = TrafficStats.getTotalTxBytes();
                dn1 = TrafficStats.getTotalRxBytes();
                up2 = TrafficStats.getTotalTxBytes();
                dn2 = TrafficStats.getTotalRxBytes();
                b = true;
            }
        }

        if(isInternetConnected(context).equals("wifi")){
            if(!sp.getBoolean("limitless_wifi", false)){
                try {
                    if(getTimeDiff("wifi")){
                        wifiMgr.setWifiEnabled(false);
                        showDisconnectInfo("Your wifi package is expired.");
                        //Log.e("Date2", "asdf");
                    }
                    if(sp.getLong("totalWifi", 0) > sp.getLong("dataLimit_wifi", 999999999) / 1000){
                        wifiMgr.setWifiEnabled(false);
                        showDisconnectInfo("Your wifi data is finished.");
                        //Log.e("Date2", "asdf2");
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }else {

            }
        }
        if(isInternetConnected(context).equals("mobile")){
            if(!sp.getBoolean("limitless_mobile", false)){
                try {
                    if(getTimeDiff("mobile")){
                        setMobileDataEnabled(context, false);
                        showDisconnectInfo("Your mobile data is expired.");
                    }
                    if(sp.getLong("totalMob", 0) > sp.getLong("dataLimit_mobile", 999999999) / 1000){
                        setMobileDataEnabled(context, false);
                        showDisconnectInfo("Your mobile data is finished.");
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }else {

            }
        }
    }

    public static String isInternetConnected(Context ctx) {
        ConnectivityManager connectivityMgr = (ConnectivityManager) ctx
                .getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        // Check if wifi or mobile network is available or not. If any of them is
        // available or connected then it will return true, otherwise false;
        if (wifi != null) {
            if (wifi.isConnected()) {
                return "wifi";
            }
        }
        if (mobile != null) {
            if (mobile.isConnected()) {
                return "mobile";
            }
        }
        return "none";
    }

    void  showDisconnectInfo(String msg){
        Intent i = new Intent(context, AlertFromService.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("msg", msg);
        context.startActivity(i);
    }

    boolean getTimeDiff(String net) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        String dateStop = "20/2/3000";

        String dateStart = format.format(c.getTime());
        if(net.equals("mobile")){
            dateStop = sp.getString("dataDuration_mobile", "0/0/0") + " 00:00:00";
            if(dateStop.equals("0/0/0 00:00:00")){
                return false;
            }
        }else if(net.equals("wifi")){
            dateStop = sp.getString("dataDuration_wifi", "0/0/0") + " 00:00:00";
            if(dateStop.equals("0/0/0 00:00:00")){
                return false;
            }
        }

        //Log.e("Date", dateStop);
        //Log.e("Date2", format.format(c.getTime()));

        // Custom date format


        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Get msec from each, and subtract.
        long diff = d2.getTime() - d1.getTime();

        //int day = (int)TimeUnit.SECONDS.toDays(diff);
        //long hours = TimeUnit.SECONDS.toHours(diff) - (day *24);
        //long minute = TimeUnit.SECONDS.toMinutes(diff) - (TimeUnit.SECONDS.toHours(diff)* 60);
        //long second = TimeUnit.SECONDS.toSeconds(diff) - (TimeUnit.SECONDS.toMinutes(diff) *60);

        //long diffSeconds = diff / 1000 % 60;
        //long diffMinutes = diff / (60 * 1000) % 60;
        //long diffHours = diff / (60 * 60 * 1000) % 24;
        //long diffDays = diff / (60 * 60 * 1000 * 24);
        return printDifference(diff, net);
    }

    public boolean printDifference(long different, String net) {
        if(net.equals("mobile")){
            if(!sp.getBoolean("limitless_mobile", false)){
                if(different < 10000){
                    return true;
                }else{
                    return false;
                }

            }else{
                return false;
            }
        }else if(net.equals("wifi")){
            if(!sp.getBoolean("limitless_wifi", false)){
                if(different < 10000){
                    return true;
                }else{
                    return false;
                }

            }else{
                return false;
            }
        }
        return false;
    }

    private void setMobileDataEnabled(Context context, boolean enabled) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InvocationTargetException {
        final ConnectivityManager conman = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final Class conmanClass = Class.forName(conman.getClass().getName());
        final Field connectivityManagerField = conmanClass.getDeclaredField("mService");
        connectivityManagerField.setAccessible(true);
        final Object connectivityManager = connectivityManagerField.get(conman);
        final Class connectivityManagerClass =  Class.forName(connectivityManager.getClass().getName());
        final Method setMobileDataEnabledMethod = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);

        setMobileDataEnabledMethod.invoke(connectivityManager, enabled);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        startService(new Intent(getApplicationContext(), Background.class));
    }

    public void freeMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }
}
