package triumphit.nets.speeds.meters;

import android.Manifest;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.TrafficStats;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class Fragment3 extends Fragment {


    private static int UID = 0;
    ListView lv;
    View v;
    ArrayList<Drawable> appLogo;
    ArrayList appName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_fragment3, container, false);
        lv = (ListView) v.findViewById(R.id.listView);
        appLogo = new ArrayList<Drawable>();
        appName = new ArrayList();
        getPakagesInfoUsingHashMap();
        lv.setAdapter(new CustomList(getContext(), appLogo, appName));
        return v;
    }

    public void getPakagesInfoUsingHashMap() {
        final PackageManager pm = getActivity().getPackageManager();
        // get a list of installed apps.
        List<ApplicationInfo> packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            // get the UID for the selected app
            UID = packageInfo.uid;
            String package_name = packageInfo.packageName;
            ApplicationInfo app = null;
            try {
                app = pm.getApplicationInfo(package_name, 0);
            } catch (PackageManager.NameNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //String name = (String) pm.getApplicationLabel(app);
            //Drawable icon = pm.getApplicationIcon(app);
            if ((app.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {

            } else {
                if(PackageManager.PERMISSION_GRANTED == pm.checkPermission(Manifest.permission.INTERNET, app.packageName)){
                    appLogo.add(pm.getApplicationIcon(app));
                    appName.add(pm.getApplicationLabel(app));
                }
            }


            // internet usage for particular app(sent and received)
            double received = (double) TrafficStats.getUidRxBytes(UID)

                    / (1024 * 1024);
            double send = (double) TrafficStats.getUidTxBytes(UID)
                    / (1024 * 1024);
            double total = received + send;
        }
    }

}
