package triumphit.nets.speeds.meters;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.util.Log;
import android.widget.RemoteViews;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Random;

/**
 * Created by Tushar on 10/27/2015.
 */
public class Disconnector extends AppWidgetProvider {
    private static final String MOBILE_CLICKED = "mobile";
    private static final String WIFI_CLICKED    = "wifi";
    private static final String PLAN_CLICKED    = "plan";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {

        // Get all ids
        ComponentName thisWidget = new ComponentName(context,
                Disconnector.class);
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        for (int widgetId : allWidgetIds) {
            // create some random data
            int number = (new Random().nextInt(100));

            RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.widget_layout);
            remoteViews.setOnClickPendingIntent(R.id.button5, getPendingSelfIntent(context, MOBILE_CLICKED));
            remoteViews.setOnClickPendingIntent(R.id.button4, getPendingSelfIntent(context, WIFI_CLICKED));
            remoteViews.setOnClickPendingIntent(R.id.button6, getPendingSelfIntent(context, PLAN_CLICKED));
            Log.w("WidgetExample", String.valueOf(number));
            // Set the text
            //remoteViews.setTextViewText(R.id.update, String.valueOf(number));

            // Register an onClickListener
            Intent intent = new Intent(context, Disconnector.class);

            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                    0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            //remoteViews.setOnClickPendingIntent(R.id.update, pendingIntent);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        super.onReceive(context, intent);
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(context);
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        RemoteViews remoteViews;
        ComponentName watchWidget;

        remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        watchWidget = new ComponentName(context, Disconnector.class);


        Log.e("Chedked Neteork:", intent.getAction());
        if(intent.getAction().equals(PLAN_CLICKED)){
            Intent i = new Intent(context, Limii.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
        if(intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")){
            if(isInternetConnected(context).equals("mobile")){
                remoteViews.setTextViewText(R.id.button4, "WIFI IS OFF");
                remoteViews.setTextViewText(R.id.button5, "DATA IS ON");
            }else if(isInternetConnected(context).equals("wifi")){
                remoteViews.setTextViewText(R.id.button4, "WIFI IS ON");
                remoteViews.setTextViewText(R.id.button5, "DATA IS OFF");
            }
            else if(isInternetConnected(context).equals("none")){
                remoteViews.setTextViewText(R.id.button4, "WIFI IS OFF");
                remoteViews.setTextViewText(R.id.button5, "DATA IS OFF");
            }
            appWidgetManager.updateAppWidget(watchWidget, remoteViews);
        }

        if (MOBILE_CLICKED.equals(intent.getAction())) {

            //remoteViews.setTextViewText(R.id.button5, "TESTING");

            if(android.os.Build.VERSION.SDK_INT <= 15){

                if(isInternetConnected(context).equals("mobile")){
                    try {
                        setMobileDataEnabled(context, false);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }else if(isInternetConnected(context).equals("none")){
                    if(telephonyInfo.isDualSIM()){
                        Intent i = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        ComponentName cName = new ComponentName("com.android.phone", "com.android.phone.Settings");
                        i.setComponent(cName);
                        context.startActivity(i);
                    }else {
                        try {
                            setMobileDataEnabled(context, true);
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        } catch (NoSuchFieldException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }
//                if(telephonyInfo.isDualSIM()){
//                    Intent i = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
//                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    ComponentName cName = new ComponentName("com.android.phone", "com.android.phone.Settings");
//                    i.setComponent(cName);
//                    context.startActivity(i);
//                }else{
//                    if(isInternetConnected(context).equals("mobile")){
//                        try {
//                            setMobileDataEnabled(context, false);
//                        } catch (ClassNotFoundException e) {
//                            e.printStackTrace();
//                        } catch (NoSuchFieldException e) {
//                            e.printStackTrace();
//                        } catch (IllegalAccessException e) {
//                            e.printStackTrace();
//                        } catch (NoSuchMethodException e) {
//                            e.printStackTrace();
//                        } catch (InvocationTargetException e) {
//                            e.printStackTrace();
//                        }
//                    }else if(isInternetConnected(context).equals("none")){
//                        try {
//                            setMobileDataEnabled(context, true);
//                        } catch (ClassNotFoundException e) {
//                            e.printStackTrace();
//                        } catch (NoSuchFieldException e) {
//                            e.printStackTrace();
//                        } catch (IllegalAccessException e) {
//                            e.printStackTrace();
//                        } catch (NoSuchMethodException e) {
//                            e.printStackTrace();
//                        } catch (InvocationTargetException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }

            }else {

                if(isInternetConnected(context).equals("mobile")){
                    try {
                        setMobileDataEnabled(context, false);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }else if(isInternetConnected(context).equals("none")){
                    if(telephonyInfo.isDualSIM()){
                        Intent i = new Intent();
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.setAction(Settings.ACTION_DATA_ROAMING_SETTINGS);
                        context.startActivity(i);
                    }else{
                        try {
                            setMobileDataEnabled(context, true);
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        } catch (NoSuchFieldException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }

//                if(telephonyInfo.isDualSIM()){
//                    Intent i = new Intent();
//                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    i.setAction(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS);
//                    context.startActivity(i);
//                }else{
//                    if(isInternetConnected(context).equals("mobile")){
//                        try {
//                            setMobileDataEnabled(context, false);
//                        } catch (ClassNotFoundException e) {
//                            e.printStackTrace();
//                        } catch (NoSuchFieldException e) {
//                            e.printStackTrace();
//                        } catch (IllegalAccessException e) {
//                            e.printStackTrace();
//                        } catch (NoSuchMethodException e) {
//                            e.printStackTrace();
//                        } catch (InvocationTargetException e) {
//                            e.printStackTrace();
//                        }
//                    }else if(isInternetConnected(context).equals("none")){
//                        try {
//                            setMobileDataEnabled(context, true);
//                        } catch (ClassNotFoundException e) {
//                            e.printStackTrace();
//                        } catch (NoSuchFieldException e) {
//                            e.printStackTrace();
//                        } catch (IllegalAccessException e) {
//                            e.printStackTrace();
//                        } catch (NoSuchMethodException e) {
//                            e.printStackTrace();
//                        } catch (InvocationTargetException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
            }
//            if(isInternetConnected(context).equals("mobile")){
//                remoteViews.setTextViewText(R.id.button5, "MOBILE DATA IS OFF");
//            }else if(isInternetConnected(context).equals("none")){
//                remoteViews.setTextViewText(R.id.button5, "MOBILE DATA IS ON");
//            }

            appWidgetManager.updateAppWidget(watchWidget, remoteViews);

        }

        if (WIFI_CLICKED.equals(intent.getAction())) {
//            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
//
//            RemoteViews remoteViews;
//            ComponentName watchWidget;

//            remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
//            watchWidget = new ComponentName(context, Disconnector.class);

            if(isInternetConnected(context).equals("wifi")){
                wifiManager.setWifiEnabled(false);
            }else if(isInternetConnected(context).equals("none")){
                wifiManager.setWifiEnabled(true);
            }


//            if(isInternetConnected(context).equals("wifi")){
//                remoteViews.setTextViewText(R.id.button4, "WIFI IS OFF");
//            }else if(isInternetConnected(context).equals("none")){
//                remoteViews.setTextViewText(R.id.button4, "WIFI IS ON");
//            }

            //appWidgetManager.updateAppWidget(watchWidget, remoteViews);
        }
    }

    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    private void setMobileDataEnabled(Context context, boolean enabled) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InvocationTargetException {
        final ConnectivityManager conman = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final Class conmanClass = Class.forName(conman.getClass().getName());
        final Field connectivityManagerField = conmanClass.getDeclaredField("mService");
        connectivityManagerField.setAccessible(true);
        final Object connectivityManager = connectivityManagerField.get(conman);
        final Class connectivityManagerClass =  Class.forName(connectivityManager.getClass().getName());
        final Method setMobileDataEnabledMethod = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);

        setMobileDataEnabledMethod.invoke(connectivityManager, enabled);
    }

    public static String isInternetConnected(Context ctx) {
        ConnectivityManager connectivityMgr = (ConnectivityManager) ctx
                .getSystemService(ctx.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        // Check if wifi or mobile network is available or not. If any of them is
        // available or connected then it will return true, otherwise false;
        if (wifi != null) {
            if (wifi.isConnected()) {
                return "wifi";
            }
        }
        if (mobile != null) {
            if (mobile.isConnected()) {
                return "mobile";
            }
        }
        return "none";
    }

}
