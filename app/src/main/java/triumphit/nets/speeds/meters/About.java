package triumphit.nets.speeds.meters;

import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class About extends AppCompatActivity {

    TextView about;
    Tracker realTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("About");

        realTime = ((Analytics)getApplication()).getTracker(Analytics.TrackerName.APP_TRACKER);
        realTime.setScreenName("About");
        realTime.send(new HitBuilders.AppViewBuilder().build());
        realTime.enableAdvertisingIdCollection(true);
        realTime.enableExceptionReporting(true);
        realTime.setAnonymizeIp(true);

        about = (TextView) findViewById(R.id.textView10);
        try {
            about.setText("Version :" + getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            about.setText("0.0.0.0");
        }
    }

}
