package triumphit.nets.speeds.meters;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootCompleted extends BroadcastReceiver {
    public BootCompleted() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, Background.class));
    }
}
