package triumphit.nets.speeds.meters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Tushar on 12/26/2015.
 */
public class CustomList extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private static Context context;
    ArrayList<Drawable> appLogo;
    ArrayList appName;

    public CustomList(Context context, ArrayList<Drawable> appLogo, ArrayList appName){
        this.context = context;
        this.appLogo = appLogo;
        this.appName = appName;
    }

    @Override
    public int getCount() {
        return appLogo.size();
    }

    @Override
    public Object getItem(int position) {
        return appLogo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        if (convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.customlistviewrow, null);
            holder = new Holder();
            holder.iv = (ImageView) convertView.findViewById(R.id.imageView6);
            holder.appTitle = (TextView) convertView.findViewById(R.id.textView21);
            holder.appDownload = (TextView) convertView.findViewById(R.id.textView22);
            holder.appUpload = (TextView) convertView.findViewById(R.id.textView23);
            convertView.setTag(holder);
        }else {
            //Log.i("TAG","inside");
            holder = (Holder) convertView.getTag();
            //rowView.setTag(holder);
        }

        holder.iv.setImageDrawable(appLogo.get(position));
        holder.appTitle.setText("" + appName.get(position));

        return convertView;
    }

    public static class Holder {
        TextView appTitle, appUpload, appDownload;
        ImageView iv;
        int position;

    }

}
