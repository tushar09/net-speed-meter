package triumphit.nets.speeds.meters;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.plus.PlusShare;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

public class SpeedTest extends AppCompatActivity implements OnComplete{

    String uploadFileUrl = "https://tushkhush.wordpress.com/2016/01/21/fileuploadurl/";
    static DownloadTheUploadURL dff;
    static DownloadFile df;
    private TextView messageText, max, maxDn, stat, average,    dnavgKB, dnavgMb, dnavgMB, upavgKB, upavgMb, upavgMB;
    static Button uploadButton, cancel;
    int serverResponseCode = 0;
    ProgressDialog dialog = null;
    final String uploadFileName = "aaa.mp3";
    final String uploadFilePath = "assets/";
    private Handler handler;
    private Runnable updateTask;
    private Handler handlerdn;
    private Runnable updateTaskdn;
    private boolean handlerCheckDn = false;
    private static boolean handlerCheck = false;
    private ProgressBar mp, mpdn;
    private AdView mAdView;
    private static RelativeLayout upContainer, dnContainer;
    static SharedPreferences sp;
    static SharedPreferences.Editor editor;
    CircleImageView share;

    Tracker realTime;
    String accountName = "not set";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed_test2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Network Speed Tester Beta");

        sp = getSharedPreferences("stat", Context.MODE_PRIVATE);
        editor = sp.edit();

        realTime = ((Analytics) getApplication()).getTracker(Analytics.TrackerName.APP_TRACKER);
        realTime.setScreenName("Testing Speed");
        realTime.send(new HitBuilders.AppViewBuilder().build());
        realTime.enableAdvertisingIdCollection(true);
        realTime.enableExceptionReporting(true);
        realTime.setAnonymizeIp(true);

        Account account = getAccount(AccountManager.get(getApplicationContext()));

        if (account == null) {
            accountName = "notSet";
        } else {
            accountName = account.name;
        }

        share = (CircleImageView) findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mp.getProgress() > mpdn.getProgress()){
                    Intent shareIntent = new PlusShare.Builder(SpeedTest.this)
                            .setText("Using " + upavgMb.getText().toString() + " network.")
                            .setType("text/plain")
                            .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=triumphit.net.speed.meters"))
                            .setContentDeepLinkId("testID",
                                    "Net Speed Meter",
                                    "Best Net Speed Meter",
                                    Uri.parse("https://play.google.com/store/apps/details?id=triumphit.net.speed.meters"))
                            .getIntent();
                    startActivityForResult(shareIntent, 0);
                    realTime.send(new HitBuilders.EventBuilder()
                            .setCategory("Speed Posted" + " speed: " + upavgKB.getText().toString())
                            .setAction(sp.getString("userEmail", "null") + " speed: " + mpdn.getProgress() + " KB/s")
                            .setLabel(sp.getString("userName", "null") + " speed: " + mpdn.getProgress() + " KB/s")
                            .build());
                }else if(mpdn.getProgress() > mp.getProgress()){
                    Intent shareIntent = new PlusShare.Builder(SpeedTest.this)
                            .setText("Using " + dnavgMb.getText().toString() + " network.")
                            .setType("text/plain")
                            .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=triumphit.net.speed.meters"))
                            .setContentDeepLinkId("testID",
                                    "Net Speed Meter",
                                    "Best NEt Speed Meter",
                                    Uri.parse("https://play.google.com/store/apps/details?id=triumphit.net.speed.meters"))
                            .getIntent();
                    startActivityForResult(shareIntent, 0);
                    realTime.send(new HitBuilders.EventBuilder()
                            .setCategory("Speed Posted" + " speed: " + dnavgKB.getText().toString())
                            .setAction(sp.getString("userEmail", "null") + " speed: " + mpdn.getProgress() + " KB/s")
                            .setLabel(sp.getString("userName", "null") + " speed: " + mpdn.getProgress() + " KB/s")
                            .build());
                }else{
                    Intent shareIntent = new PlusShare.Builder(SpeedTest.this)
                            .setText("Using " + dnavgMb.getText().toString() + " network.")
                            .setType("text/plain")
                            .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=triumphit.net.speed.meters"))
                            .setContentDeepLinkId("testID",
                                    "Net Speed Meter",
                                    "Best NEt Speed Meter",
                                    Uri.parse("https://play.google.com/store/apps/details?id=triumphit.net.speed.meters"))
                            .getIntent();
                    startActivityForResult(shareIntent, 0);
                    realTime.send(new HitBuilders.EventBuilder()
                            .setCategory("Speed Posted" + " speed: " + dnavgKB.getText().toString())
                            .setAction(sp.getString("userEmail", "null") + " speed: " + mpdn.getProgress() + " KB/s")
                            .setLabel(sp.getString("userName", "null") + " speed: " + mpdn.getProgress() + " KB/s")
                            .build());
                }



            }
        });

        final OnComplete oc = this;

        dff = new DownloadTheUploadURL();
        dff.execute(uploadFileUrl);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
        Typeface type = Typeface.createFromAsset(getAssets(), "Capture it.ttf");

        mp = (ProgressBar) findViewById(R.id.progressBar2);
        mpdn = (ProgressBar) findViewById(R.id.progressBar3);

        dnavgKB = (TextView) findViewById(R.id.textView29);
        dnavgMb = (TextView) findViewById(R.id.textView31);
        dnavgMB = (TextView) findViewById(R.id.textView32);
        upavgKB = (TextView) findViewById(R.id.textView30);
        upavgMb = (TextView) findViewById(R.id.textView33);
        upavgMB = (TextView) findViewById(R.id.textView34);

        upContainer = (RelativeLayout) findViewById(R.id.relativeLayout4);
        dnContainer = (RelativeLayout) findViewById(R.id.relativeLayout6);
        dnContainer.setVisibility(View.INVISIBLE);
        upContainer.setVisibility(View.INVISIBLE);

        uploadButton = (Button) findViewById(R.id.uploadButton);
        cancel = (Button) findViewById(R.id.cancel);
        messageText = (TextView) findViewById(R.id.messageText);
        max = (TextView) findViewById(R.id.textView25);
        maxDn = (TextView) findViewById(R.id.textView37);
        average = (TextView) findViewById(R.id.textView24);
        average.setTypeface(type);

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(SpeedTest.this);
                builder.setTitle("Mobile Data Saver")
                        .setMessage("Testing your network speed will cost approximately 6 MB. Once we start to monitor upload speed you can not cancel the process. Proceed?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                deleteCache(SpeedTest.this);
                                df = new DownloadFile(SpeedTest.this, oc);
                                df.execute("http://static1.1.sqspcdn.com/static/f/157301/2327490/1231167410583/traffic_pub_gen19.pdf?token=d5TR8YssQ0SWWZgDpR2pxYEAUWQ%3D");
                                messageText.setText("Downloading...");
                                handlerdn = new Handler();
                                updateTaskdn = new Runnable() {
                                    @Override
                                    public void run() {
                                        countReceivedData();
                                        handlerdn.postDelayed(this, 1000);
                                    }
                                };
                                if (!handlerCheckDn) {
                                    handlerdn.postDelayed(updateTaskdn, 0);
                                    handlerCheckDn = true;
                                }
                                realTime.send(new HitBuilders.EventBuilder()
                                        .setCategory("Started" + " speed meater")
                                        .setAction(sp.getString("userEmail", "null"))
                                        .setLabel(sp.getString("userName", "null"))
                                        .build());
                            }
                        })
                        .setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(R.drawable.alert)
                        .show();

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putBoolean("contDown", false);
                editor.putBoolean("contUp", false);
                editor.commit();

                df.cancel(true);
                dff.cancel(true);
            }
        });

    }

    public static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }
    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        }
        else if(dir!= null && dir.isFile())
            return dir.delete();
        else {
            return false;
        }
    }


    long upMax = 0;
    long up = 0;

    long up1 = 0;
    long up2 = 0;

    boolean b = false;
    boolean firstTime = false;
    long time = 1;
    boolean ok = true;

    private void countTransmitedData() {
        if(ok){
            if(firstTime == true){
                if (b) {
                    up1 = TrafficStats.getTotalTxBytes();
                    b = false;
                } else {
                    up2 = TrafficStats.getTotalTxBytes();
                    b = true;
                }
                if(upMax < (Math.abs(up1 - up2) / (1000))){
                    upMax = (Math.abs(up1 - up2) / (1000));
                }
                up += (Math.abs(up1 - up2) / (1000));
                //String s = "" + (Math.abs(up1 - up2) / (1000));
                //getSupportActionBar().setTitle("" + up);
                mp.setMax((int) upMax);
                mp.setProgress((int) (up / time++));
                max.setText("MAX:" + upMax + " KB/S");
                average.setText("Average: " + mp.getProgress() + " KB/S");
                if(up > 6000){
                    ok = false;
                    share.setVisibility(View.VISIBLE);
                    dnContainer.setVisibility(View.VISIBLE);
                    upContainer.setVisibility(View.VISIBLE);
                    handler.removeCallbacks(updateTask);

                    dnavgKB.setText(mpdn.getProgress() + " KB/s");
                    dnavgMB.setText(((float)mpdn.getProgress() / 1000.00f) + " MB/s");
                    dnavgMb.setText(((float)mpdn.getProgress() / 125.00f) + " Mb/s");

                    upavgKB.setText(mp.getProgress() + " KB/s");
                    upavgMB.setText(((float)mp.getProgress() / 1000.00f) + " MB/s");
                    upavgMb.setText(((float)mp.getProgress() / 125.00f) + " Mb/s");

                    realTime.send(new HitBuilders.EventBuilder()
                            .setCategory("Upload Speed" + " speed: " + mp.getProgress() + " KB/s")
                            .setAction(sp.getString("userEmail", "null") + " speed: " + mp.getProgress() + " KB/s")
                            .setLabel(sp.getString("userName", "null") + " speed: " + mp.getProgress() + " KB/s")
                            .build());
                }
            }else{
                firstTime = true;
                if (b) {
                    up1 = TrafficStats.getTotalTxBytes();
                    up2 = TrafficStats.getTotalTxBytes();
                    b = false;
                } else {
                    up2 = TrafficStats.getTotalTxBytes();
                    up1 = TrafficStats.getTotalTxBytes();
                    b = true;
                }
            }
        }

    }

    long dnMax = 0;
    long dn = 0;
    long dn1 = 0;
    long dn2 = 0;
    boolean c = false;
    boolean firstTimedn = false;
    long timeDn = 1;
    boolean okdn = true;
    private void countReceivedData() {
        if(okdn){
            if(firstTimedn == true){
                if (c) {
                    dn1 = TrafficStats.getTotalRxBytes();
                    c = false;
                } else {
                    dn2 = TrafficStats.getTotalRxBytes();
                    c = true;
                }
                if(dnMax < (Math.abs(dn1 - dn2) / (1000))){
                    dnMax = (Math.abs(dn1 - dn2) / (1000));
                }
                dn += (Math.abs(dn1 - dn2) / (1000));
                //String s = "" + (Math.abs(up1 - up2) / (1000));
                //getSupportActionBar().setTitle("" + dn);
                mpdn.setMax((int) dnMax);
                mpdn.setProgress((int) (dn / timeDn++));
                maxDn.setText("MAX:" + dnMax + " KB/S");
                average.setText("Average: " + mpdn.getProgress() + " KB/S");
//                if(dn > 7200){
//                    okdn = false;
//                    dnContainer.setVisibility(View.VISIBLE);
//                    upContainer.setVisibility(View.VISIBLE);
//                    handler.removeCallbacks(updateTask);
//                }
            }else{
                firstTimedn = true;
                if (c) {
                    dn1 = TrafficStats.getTotalRxBytes();
                    dn2 = TrafficStats.getTotalRxBytes();
                    c = false;
                } else {
                    dn2 = TrafficStats.getTotalRxBytes();
                    dn1 = TrafficStats.getTotalRxBytes();
                    c = true;
                }
            }
        }


    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(SpeedTest.this).reportActivityStop(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        ok = true;
        time = 1;
        upMax = 0;
        up = 0;
        handlerCheck = false;

        okdn = true;
        timeDn = 1;
        dnMax = 0;
        dn = 0;
        handlerCheckDn = false;
        GoogleAnalytics.getInstance(SpeedTest.this).reportActivityStart(this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        editor.putBoolean("contDown", true);
        editor.putBoolean("contUp", true);
        editor.commit();
        if(df != null){
            df.cancel(true);
        }
        if(dff != null){
            dff.cancel(true);
        }

        finish();
    }

    public int uploadFile(String sourceFileUri) {

        File fileBrochure = new File(Environment.getExternalStorageDirectory().getPath() + "/" + "aaa.mp3");
        if (!fileBrochure.exists()) {
            CopyAssetsbrochure();
        }

        //File file = new File(Environment.getExternalStorageDirectory().getPath() + "/" + "aaa.mp3");
        String fileName = Environment.getExternalStorageDirectory().getPath() + "/" + "aaa.mp3";
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(Environment.getExternalStorageDirectory().getPath() + "/" + "aaa.mp3");

        if (!sourceFile.isFile()) {

            //dialog.dismiss();

            Log.e("uploadFile", "Source File not exist :"
                    + "http://beatbox.ga/url/UploadToServer.php" + " " + uploadFileName);

            runOnUiThread(new Runnable() {
                public void run() {
                    messageText.setText("Source File not exist :"
                            + uploadFilePath + "" + uploadFileName);
                }
            });

            return 0;

        } else {
            try {

                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(dff.url);

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                        + fileName + "\"" + lineEnd);

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while ((sp.getBoolean("contUp", true)) && (bytesRead > 0)) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {

                    runOnUiThread(new Runnable() {
                        public void run() {

//                            String msg = "File Upload Completed.\n\n See uploaded file here : \n\n"
//                                    + " http://www.androidexample.com/media/uploads/"
//                                    + uploadFileName;

                            messageText.setText("");
                            Toast.makeText(SpeedTest.this, "File Upload Complete.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (MalformedURLException ex) {

                //dialog.dismiss();
                ex.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        messageText.setText("MalformedURLException Exception : check script url.");
                        Toast.makeText(SpeedTest.this, "MalformedURLException",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {

                //dialog.dismiss();
                e.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        messageText.setText("Got Exception : see logcat ");
                        Toast.makeText(SpeedTest.this, "Got Exception : see logcat ",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                Log.e("Upload file to server", "Exception : "
                        + e.getMessage(), e);
            }
            //dialog.dismiss();
            return serverResponseCode;

        } // End else block
    }

    private void CopyAssetsbrochure() {
        AssetManager assetManager = getAssets();
        String[] files = null;
        try {
            files = assetManager.list("");
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }
        for (int i = 0; i < files.length; i++) {
            String fStr = files[i];
            if (fStr.equalsIgnoreCase("aaa.mp3")) {
                InputStream in = null;
                OutputStream out = null;
                try {
                    in = assetManager.open(files[i]);
                    out = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/" + files[i]);
                    copyFile(in, out);
                    in.close();
                    in = null;
                    out.flush();
                    out.close();
                    out = null;
                    break;
                } catch (Exception e) {
                    Log.e("tag", e.getMessage());
                }
            }
        }
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @Override
    public void onDownloadComplete(String result) {
        Log.e("Deleted", "Done");
        realTime.send(new HitBuilders.EventBuilder()
                .setCategory("Download Speed" + " speed: " + mpdn.getProgress() + " KB/s")
                .setAction(sp.getString("userEmail", "null") + " speed: " + mpdn.getProgress() + " KB/s")
                .setLabel(sp.getString("userName", "null") + " speed: " + mpdn.getProgress() + " KB/s")
                .build());
        okdn = false;
        cancel.setEnabled(false);
        cancel.setText("...");

        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        messageText.setText("uploading started.....");
                    }
                });
                uploadFile(uploadFilePath + "" + uploadFileName);

            }
        }).start();

        handler = new Handler();
        updateTask = new Runnable() {
            @Override
            public void run() {
                countTransmitedData();
                handler.postDelayed(this, 1000);
            }
        };
        if (!handlerCheck) {
            handler.postDelayed(updateTask, 0);
            handlerCheck = true;
        }

    }
}