package triumphit.nets.speeds.meters;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Limii extends AppCompatActivity {

    Spinner dataUnit;
    TextView dateShow, applyInfo;
    EditText dataVolume, expire;
    List<String> unit;
    List<String> days;
    DatePicker dp;
    Button apply;
    RadioGroup rg;
    RadioButton rb;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    public static Background service;
    private ToolTipRelativeLayout mToolTipFrameLayout;
    private ToolTipView myToolTipView;
    Tracker realTime;
    private boolean shouldShoAdd = false;
    private String accountName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changed_limit);

        realTime = ((Analytics) getApplication()).getTracker(Analytics.TrackerName.APP_TRACKER);
        realTime.setScreenName("Set Plan");
        realTime.send(new HitBuilders.AppViewBuilder().build());
        realTime.enableAdvertisingIdCollection(true);
        realTime.enableExceptionReporting(true);
        realTime.setAnonymizeIp(true);

        final TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);

        Account account = getAccount(AccountManager.get(getApplicationContext()));
        final String accountName;
        if(account == null){
            accountName = "notSet";
        } else{
            accountName = account.name;
        }

        final ToolTipRelativeLayout toolTipRelativeLayout = (ToolTipRelativeLayout) findViewById(R.id.activity_main_tooltipRelativeLayout);

        final ToolTip toolTip = new ToolTip()
                .withText("Provide how many days are left when the data volume will be expired. Write negative value to set it unlimited.")
                .withColor(Color.parseColor("#00A5EC"))
                .withTextColor(Color.parseColor("#ffffff"))
                .withShadow()
                .withAnimationType(ToolTip.AnimationType.FROM_TOP);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00A5EC")));
        getSupportActionBar().setTitle("Plan");

        dataUnit = (Spinner) findViewById(R.id.spinner);
        dateShow = (TextView) findViewById(R.id.textView);
        rg = (RadioGroup) findViewById(R.id.rg);
        applyInfo = (TextView) findViewById(R.id.textView3);


        service = new Background();
        sp = getSharedPreferences("stat", Context.MODE_PRIVATE);
        editor = sp.edit();

        //cur_volume.setText("Total: " + (sp.getLong("dataLimit", 0) / 1000) + "MB");
        calculateAvailability();


        dataVolume = (EditText) findViewById(R.id.editText);
        expire = (EditText) findViewById(R.id.editText2);
        apply = (Button) findViewById(R.id.button3);

        unit = new ArrayList<String>();
        unit.add("MB");
        unit.add("GB");
        unit.add("Unlimited");

        dateShow.setText(getDateTimeTemp(0));

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, unit);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dataUnit.setAdapter(dataAdapter2);

        dataUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 2) {
                    dataVolume.setText("Unlimited");
                    dataVolume.setEnabled(false);
                } else {
                    dataVolume.setEnabled(true);
                    dataVolume.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        expire.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    dateShow.setText(getDateTimeTemp(0));
                } else {
                    if (!s.toString().equals("-") && !s.toString().equals("+")) {
                        if (Integer.parseInt(s.toString()) < 0) {
                            dateShow.setText("Unlimited");
                        } else {
                            dateShow.setText(getDateTimeTemp(getDuration()));
                        }
                    }
                }
            }
        });

        expire.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                expire.requestFocus();
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (expire.getRight() - expire.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        myToolTipView = toolTipRelativeLayout.showToolTipForView(toolTip, findViewById(R.id.editText2));
                        expire.clearFocus();
                        myToolTipView.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
                            @Override
                            public void onToolTipViewClicked(ToolTipView toolTipView) {

                            }
                        });
                        return true;
                    }
                }
                return false;
            }
        });

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyInfo.setVisibility(View.VISIBLE);
                boolean data = false, date = false;
                //Log.e("volume", dataVolume.getText().toString());
                //Log.e("date", expire.getText().toString());
                if (dataVolume.getText().toString().equals("")) {
                    data = false;
                } else if (dataVolume.getText().toString().equals("Unlimited")) {
                    data = true;
                } else {
                    data = true;
                }

                if (expire.getText().toString().equals("")) {
                    date = false;
                } else if (expire.getText().toString().equals("-1")) {
                    date = true;
                } else {
                    date = true;
                }

                int selected_radio_button = rg.getCheckedRadioButtonId();
                rb = (RadioButton) findViewById(selected_radio_button);

                Log.e("Stat", "data: " + data + " date: " + date + " net: " + rb.getText());
                if (data == true && date == true) {

                    new AlertDialog.Builder(Limii.this)
                            .setTitle("Apply Plan")
                            .setMessage("Are you sure you want to apply this plan? Previous plan will be lost")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (rb.getText().toString().equals("Mobile Data")) {
                                        editor.putLong("dataLimit_mobile", getNewDtaVolume());
                                        editor.putString("dataDuration_mobile", getDateTime(getDuration()));
                                    } else if (rb.getText().toString().equals("Wifi Data")) {
                                        editor.putLong("dataLimit_wifi", getNewDtaVolume());
                                        editor.putString("dataDuration_wifi", getDateTime(getDuration()));
                                    }

                                    setNetworkType();
                                    editor.putBoolean("planSetted", true);
                                    editor.commit();
                                    applyInfo.setTextColor(Color.parseColor("#189230"));
                                    applyInfo.setText("Info: Plan is activated successfully. This device will be disconnected from internet after the data volume finished or just before the day " + getDateTime(getDuration()) + " arrive");

                                    realTime.send(new HitBuilders.EventBuilder()
                                            .setCategory("Plan Setup" + rb.getText().toString())
                                            .setAction(sp.getString("userName", "null") + " volume: " + sp.getLong("dataLimit_mobile", 0))
                                            .setLabel(sp.getString("userEmail", "null") + " days: " + getDuration())
                                            .build());

                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                } else {
                    applyInfo.setTextColor(Color.RED);
                    applyInfo.setText("Please provide the volume of data and it's expire duration.");
                }


            }
        });

    }

    private void calculateAvailability() {
        long limit = sp.getLong("dataLimit", 0) / 1000;

        Log.e("Limit", "" + limit);
        long used;

        if (sp.getString("networkType", "both").equals("mobile")) {
            used = sp.getLong("totalMob", 0) / 1000;

        } else if (sp.getString("networkType", "both").equals("wifi")) {
            used = sp.getLong("totalWifi", 0);
        } else {
            Log.e("From", "Here");
            used = (sp.getLong("totalMob", 0)) + (sp.getLong("totalWifi", 0));
            Log.e("Used", "" + used);
        }
    }

    String getDateTimeTemp(int t){
        int selected_radio_button = rg.getCheckedRadioButtonId();
        rb = (RadioButton) findViewById(selected_radio_button);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        if (t == 0) {
            c.add(Calendar.DATE, t + 1); // Adding 5 days
//            if (rb.getText().toString().equals("Mobile Data")) {
//                editor.putBoolean("limitless_mobile", false);
//                editor.commit();
//            } else {
//                editor.putBoolean("limitless_wifi", false);
//                editor.commit();
//            }
        } else if (t > 0) {
            c.add(Calendar.DATE, t); // Adding 5 days
//            if (rb.getText().toString().equals("Mobile Data")) {
//                editor.putBoolean("limitless_mobile", false);
//                editor.commit();
//            } else {
//                editor.putBoolean("limitless_wifi", false);
//                editor.commit();
//            }
        } else {
//            if (rb.getText().toString().equals("Mobile Data")) {
//                editor.putBoolean("limitless_mobile", true);
//                editor.commit();
//            } else {
//                editor.putBoolean("limitless_wifi", true);
//                editor.commit();
//            }

        }

        //String output = sdf.format(c.getTime());
        //System.out.println(output);
        //dateShow.setText(sdf.format(c.getTime()));
        return sdf.format(c.getTime());
    }


    private String getDateTime(int t) {
        int selected_radio_button = rg.getCheckedRadioButtonId();
        rb = (RadioButton) findViewById(selected_radio_button);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        if (t == 0) {
            c.add(Calendar.DATE, t + 1); // Adding 5 days
            if (rb.getText().toString().equals("Mobile Data")) {
                editor.putBoolean("limitless_mobile", false);
                editor.commit();
            } else {
                editor.putBoolean("limitless_wifi", false);
                editor.commit();
            }
        } else if (t > 0) {
            c.add(Calendar.DATE, t); // Adding 5 days
            if (rb.getText().toString().equals("Mobile Data")) {
                editor.putBoolean("limitless_mobile", false);
                editor.commit();
            } else {
                editor.putBoolean("limitless_wifi", false);
                editor.commit();
            }
        } else {
            if (rb.getText().toString().equals("Mobile Data")) {
                editor.putBoolean("limitless_mobile", true);
                editor.commit();
            } else {
                editor.putBoolean("limitless_wifi", true);
                editor.commit();
            }

        }

        //String output = sdf.format(c.getTime());
        //System.out.println(output);
        //dateShow.setText(sdf.format(c.getTime()));
        return sdf.format(c.getTime());
    }

    private void setMobileDataEnabled(Context context, boolean enabled) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InvocationTargetException {
        final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final Class conmanClass = Class.forName(conman.getClass().getName());
        final Field connectivityManagerField = conmanClass.getDeclaredField("mService");
        connectivityManagerField.setAccessible(true);
        final Object connectivityManager = connectivityManagerField.get(conman);
        final Class connectivityManagerClass = Class.forName(connectivityManager.getClass().getName());
        final Method setMobileDataEnabledMethod = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);

        setMobileDataEnabledMethod.invoke(connectivityManager, enabled);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    public long getNewDtaVolume() throws NumberFormatException{
        String unit = dataUnit.getSelectedItem().toString();
        if (unit.equals("MB")) {
            return (Long.parseLong(dataVolume.getText().toString())) * 1000;
        } else if (unit.equals("GB")) {
            return (Long.parseLong(dataVolume.getText().toString())) * 1000 * 1000;
        } else {
            return 500 * 1000 * 1000;
        }

    }

    public int getDuration() {
        return Integer.parseInt(expire.getText().toString());
    }

    public void setNetworkType() {
        if (rb.getText().toString().equals("Mobile Data")) {
            editor.putString("networkType", "mobile");
            editor.putLong("totalMob", 0);
            editor.putLong("totalDownMob", 0);
            editor.putLong("totalMob", 0);
            editor.putLong("totalUpMob", 0);
            service.totalMob = 0;
            service.totalDownMob = 0;
            service.totalUpMob = 0;
            editor.commit();
        } else if (rb.getText().toString().equals("Wifi Data")) {
            editor.putString("networkType", "wifi");
            editor.putLong("totalWifi", 0);
            editor.putLong("totalDownWifi", 0);
            editor.putLong("totalWifi", 0);
            editor.putLong("totalUpWifi", 0);
            service.totalWifi = 0;
            service.totalDownWifi = 0;
            service.totalUpWifi = 0;
            editor.commit();
        }
    }

    public Account getAccount(AccountManager accountManager) {
        Account account = null;
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.GET_ACCOUNTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.GET_ACCOUNTS)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.GET_ACCOUNTS},
                        99);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else {
            Account[] accounts = accountManager.getAccountsByType("com.google");

            if (accounts.length > 0) {
                account = accounts[0];
            } else {
                account = null;
            }
        }

        return account;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Account account = getAccount(AccountManager.get(getApplicationContext()));
            if (account == null) {
                accountName = "notSet";
            } else {
                accountName = account.name;
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(Limii.this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(Limii.this).reportActivityStop(this);
    }
}
