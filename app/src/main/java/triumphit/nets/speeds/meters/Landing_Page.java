package triumphit.nets.speeds.meters;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.pushbots.push.Pushbots;
import com.squareup.picasso.Picasso;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


public class Landing_Page extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

    static SharedPreferences sp;
    static SharedPreferences.Editor editor;
    static TextView actionBarUsedText, actionBarAvailabeText, actionBarTotalText, actionBarDateText, appName, appVersion;
    static View mCustomView;
    static ImageView proPic;
    static Button signin;
    Tracker realTime;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    int pos = 0;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    ImageView header;
    ImageView civ;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;


    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean signedInUser;
    private ConnectionResult mConnectionResult;

    private static final int REQUEST_CODE_CAPTURE_IMAGE = 1;
    private static final int REQUEST_CODE_CREATOR = 2;
    private static final int REQUEST_CODE_RESOLUTION = 3;
    String accountName = "not set";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_holder);
        sp = getSharedPreferences("stat", Context.MODE_PRIVATE);
        editor = sp.edit();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);
        mNavigationView.setItemIconTintList(null);

        View child = mNavigationView.getHeaderView(0);
        header = (ImageView) child.findViewById(R.id.imageView7);
        appName = (TextView) child.findViewById(R.id.username);
        appVersion = (TextView) child.findViewById(R.id.email);
        proPic = (ImageView) child.findViewById(R.id.profile_image);
        //civ = (ImageView) child.findViewById(R.id.asd);
        Typeface type = Typeface.createFromAsset(getAssets(), "Capture it.ttf");
        appName.setTypeface(type);
        appVersion.setTypeface(type);
        try {
            appVersion.setText("Version: " + this.getPackageManager()
                    .getPackageInfo(this.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            appVersion.setText("Not detected");
        }


        StartAppSDK.init(this, "204684718", true);
        StartAppAd.disableSplash();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    //Log.e("firebase", "onAuthStateChanged:signed_in:" + user.get.getUid());
                    Log.e("firebase", "onAuthStateChanged:signed_in:" + user.getDisplayName());
                    //Log.e("firebase", "onAuthStateChanged:signed_in:" + user.get.getUid());
                    //Log.e("firebase", "onAuthStateChanged:signed_in:" + user.get.getUid());
                    //Log.e("firebase", "onAuthStateChanged:signed_in:" + user.get.getUid());
                    //makeSignUp(user.getDisplayName(), user.getEmail(), user.getPhotoUrl().toString().replaceAll("s96-c", "s720-c"), "", false);

                    Picasso.with(Landing_Page.this).load(user.getPhotoUrl().toString().replaceAll("s96-c", "s720-c")).into(proPic);
                    appName.setText(user.getDisplayName());
                    realTime.send(new HitBuilders.EventBuilder()
                            .setCategory("Logged in")
                            .setAction("Logged in " + user.getEmail())
                            .setLabel("Logged in " + user.getDisplayName())
                            .build());
                    signin.setText("SIGN OUT");
                } else {
                    signin.setText("SIGN IN");
                    // User is signed out
                    Log.e("firebase", "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("@string/app_name");


        LayoutInflater mInflater = LayoutInflater.from(this);
        mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        getSupportActionBar().setCustomView(mCustomView);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        actionBarUsedText = (TextView) mCustomView.findViewById(R.id.textView5);
        actionBarTotalText = (TextView) mCustomView.findViewById(R.id.textView4);
        actionBarAvailabeText = (TextView) mCustomView.findViewById(R.id.textView6);
        actionBarDateText = (TextView) mCustomView.findViewById(R.id.textView7);

        signin = (Button) findViewById(R.id.btn_sign_in);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (signin.getText().equals("SIGN OUT")) {
                    mAuth.signOut();
                    realTime.send(new HitBuilders.EventBuilder()
                            .setCategory("Logged out")
                            .setAction("Logged out " + sp.getString("userEmail", "null"))
                            .setLabel("Logged out " + sp.getString("userName", "null"))
                            .build());
                    signin.setText("SIGN IN");
                    //editor.putBoolean("isSignedIn", false);

                } else {
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                    //editor.putBoolean("isSignedIn", true);
                }

            }
        });



        //View child = getLayoutInflater().inflate(R.layout.temp, null);

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, new TabFragment()).commit();
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();

                if (menuItem.getItemId() == 1) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerView, new TabFragment()).commit();

                }

                if (menuItem.getItemId() == 2) {
                    FragmentTransaction xfragmentTransaction = mFragmentManager.beginTransaction();
                    xfragmentTransaction.replace(R.id.containerView, new TabFragment()).commit();
                }

                return false;
            }

        });


        //appName.setText("sadfasdf");

        final List<MenuItem> items = new ArrayList<>();
        Menu menu;
        menu = mNavigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            items.add(menu.getItem(i));
        }
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.rate) {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        realTime.send(new HitBuilders.EventBuilder()
                                .setCategory("Trying to rate")
                                .setAction(sp.getString("userEmail", "null"))
                                .setLabel(sp.getString("userName", "null"))
                                .build());
                        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {

                        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
                        //String url = "http://www.amazon.com/TriumphIT-Music-Player/dp/B00ZD2D1T2/ref=sr_1_16?s=mobile-apps&ie=UTF8&qid=1434885049&sr=1-16&keywords=amazon+mp3&pebp=1434885051870&perid=10M3Z3SR7RE5RBMZ1M1N";
                        //Intent i = new Intent(Intent.ACTION_VIEW);
                        //i.setData(Uri.parse(url));
                        //startActivity(i);
                    }
                }
                if (menuItem.getItemId() == R.id.about) {
                    startActivity(new Intent(Landing_Page.this, About.class));
                }
                if (menuItem.getItemId() == R.id.speedtest) {
                    startActivity(new Intent(Landing_Page.this, SpeedTest.class));
                    realTime.send(new HitBuilders.EventBuilder()
                            .setCategory("Speed test Activity opened")
                            .setAction(sp.getString("userEmail", "null"))
                            .setLabel(sp.getString("userName", "null"))
                            .build());
                }
                if (menuItem.getItemId() == R.id.privacy_policy) {
                    startActivity(new Intent(Landing_Page.this, SpeedTest.class));
                    realTime.send(new HitBuilders.EventBuilder()
                            .setCategory("Speed test Activity opened")
                            .setAction(sp.getString("userEmail", "null"))
                            .setLabel(sp.getString("userName", "null"))
                            .build());
                }
                return false;
            }
        });

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();


//        AdBuddiz.setPublisherKey("44bcf836-b949-40ab-994b-e1fe37d007b0");
//        AdBuddiz.cacheAds(this);
//        if(AdBuddiz.isReadyToShowAd(this)){
//            AdBuddiz.showAd(this);
//        }


        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);


        realTime = ((Analytics) getApplication()).getTracker(Analytics.TrackerName.APP_TRACKER);
        realTime.setScreenName("Main");
        realTime.send(new HitBuilders.AppViewBuilder().build());
        realTime.enableAdvertisingIdCollection(true);
        realTime.enableExceptionReporting(true);
        realTime.setAnonymizeIp(true);

        Account account = getAccount(AccountManager.get(getApplicationContext()));

        if (account == null) {
            accountName = "notSet";
        } else {
            accountName = account.name;
        }

        Pushbots.sharedInstance().init(this);
        Pushbots.sharedInstance().setAlias(accountName + " " + Build.DEVICE + " " + Build.MODEL + " Dual " + telephonyInfo.isDualSIM());


        //((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00A5EC")));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();


        startService(new Intent(getApplicationContext(), Background.class));
    }

    @Override
    protected void onActivityResult(
            final int requestCode,
            final int resultCode,
            final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                // ...
            }
        }

    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        Log.e("firebase", "firebaseAuthWithGoogle:" + acct.getPhotoUrl().toString().replaceAll("s96-c", "s720-c"));
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.e("firebase", "signInWithCredential:onComplete:" + task.isSuccessful());
                        realTime.send(new HitBuilders.EventBuilder()
                                .setCategory("Logged in")
                                .setAction("Logged in " + acct.getEmail())
                                .setLabel("Logged in " + acct.getDisplayName())
                                .build());
                        signin.setText("SIGN IN");

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.e("firebase", "signInWithCredential", task.getException());
                            Toast.makeText(Landing_Page.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        // ...
                    }
                });
    }


    public Account getAccount(AccountManager accountManager) {
        Account account = null;
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.GET_ACCOUNTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.GET_ACCOUNTS)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.GET_ACCOUNTS},
                        99);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else {
            Account[] accounts = accountManager.getAccountsByType("com.google");

            if (accounts.length > 0) {
                account = accounts[0];
            } else {
                account = null;
            }
        }

        return account;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Account account = getAccount(AccountManager.get(getApplicationContext()));
            if (account == null) {
                accountName = "notSet";
            } else {
                accountName = account.name;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(Landing_Page.this, About.class));
            //mGoogleApiClient.connect();
            return true;
        }
        if (id == R.id.action_rate) {
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
            return true;
        }
        if (id == android.R.id.home) {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
            return true;

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        setActionBarTitle(pos);
        super.onStart();
        GoogleAnalytics.getInstance(Landing_Page.this).reportActivityStart(this);
        if (sp.getBoolean("planSetted", false)) {
            editor.putBoolean("planSetted", false);
            editor.commit();
        }
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(Landing_Page.this).reportActivityStop(this);
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    public void setActionBarTitle(int pos) {
        if (pos == 0) {
            long limit = sp.getLong("dataLimit_mobile", 0) / 1000;
            long used = sp.getLong("totalMob", 0) / 1000;
            if (used > 1000) {
                double d = used / 1000.0;
                float usedPercent = getPercentage(d, limit);
                float availablePercent = 100.00f - usedPercent;
                actionBarUsedText.setText("Used: " + String.format("%.1f", usedPercent) + "%");
                actionBarAvailabeText.setText("Available: " + String.format("%.1f", availablePercent) + "%");
            } else {
                float usedPercent = getPercentage(used, limit);
                float availablePercent = 100.00f - usedPercent;

                //float availablePercent = 100.00f - usedPercent;
                actionBarUsedText.setText("Used: " + String.format("%.1f", usedPercent) + "%");
                actionBarAvailabeText.setText("Available: " + String.format("%.1f", availablePercent) + "%");
            }


            if (limit > 1000) {
                double d = limit / 1000.0;
                actionBarTotalText.setText("Total: " + d + "GB.");
            } else if ((limit - used) < 0) {
                actionBarAvailabeText.setText("Available: Over");
                actionBarUsedText.setText("Used: All");
                //actionBarUsedText.setText("Used: " + used + "%");
            } else {
                actionBarTotalText.setText("Total: " + limit + " MB.");
            }


            //actionBarAvailabeText.setText("Availabe: " + (limit - used) + " MB.");
            //actionBarTotalText.setText("Total: " + limit + " MB.");
            actionBarDateText.setText("Expire: " + sp.getString("dataDuration_mobile", "NO Limit"));
            //Log.e("Position", "" + position);
            //actionBarDateText.setText("" + n++);
        } else if (pos == 1) {
            long limit = sp.getLong("dataLimit_wifi", 0) / 1000;
            long used = sp.getLong("totalWifi", 0);
            if (used > 1000) {
                double d = used / 1000.0;
                float usedPercent = getPercentage(d, limit);
                float availablePercent = 100.00f - usedPercent;
                actionBarUsedText.setText("Used: " + String.format("%.1f", usedPercent) + "%");
                actionBarAvailabeText.setText("Available: " + String.format("%.1f", availablePercent) + "%");
                //actionBarUsedText.setText("Used: " + d + "%");
            } else {
                float usedPercent = getPercentage(used, limit);
                float availablePercent = 100.00f - usedPercent;

                //float availablePercent = 100.00f - usedPercent;
                actionBarUsedText.setText("Used: " + String.format("%.1f", usedPercent) + "%");
                actionBarAvailabeText.setText("Available: " + String.format("%.1f", availablePercent) + "%");
            }

//            if((limit - used) > 1000){
//                double d = (limit - used);
//                actionBarAvailabeText.setText("Available: " + getPercentage(d, limit) + "%");
//
//                //Log.e("percent", ""+d);
//            }else if((limit - used) < 0){
//                actionBarAvailabeText.setText("Available: Over");
//                actionBarUsedText.setText("Used: All");
//                //actionBarUsedText.setText("Used: " + used + "%");
//            }
//            else{
//                actionBarAvailabeText.setText("Available: " + getPercentage((limit - used), limit) + "%");
//                //actionBarAvailabeText.setText("Availabe: " + getPercentage((limit - used), limit) + "%");
//            }

            if (limit > 1000) {
                double d = limit / 1000.0;
                actionBarTotalText.setText("Total: " + d + "GB.");
            } else {
                actionBarTotalText.setText("Total: " + limit + " MB.");
            }
            actionBarDateText.setText("Expire: " + sp.getString("dataDuration_wifi", "NO Limit"));
            //Log.e("Position", "" + position);

        } else {
            long limit = sp.getLong("dataLimit_both", 0) / 1000;
            long used = (sp.getLong("totalMob", 0)) + (sp.getLong("totalWifi", 0)) / 1000;
            if (used > 1000) {
                double d = used / 1000.0;
                actionBarUsedText.setText("Used: " + getPercentage(d, limit) + "GB.");
            } else {
                actionBarUsedText.setText("Used: " + getPercentage(used, limit) + "MB.");
            }

            if ((limit - used) > 1000) {
                double d = (limit - used) / 1000.0;
                actionBarAvailabeText.setText("Availabe: " + getPercentage(d, limit) + "GB.");
            } else {
                actionBarAvailabeText.setText("Availabe: " + getPercentage((limit - used), limit) + " MB.");
            }

            if (limit > 1000) {
                double d = limit / 1000.0;
                actionBarTotalText.setText("Total: " + d + "GB.");
            } else {
                actionBarTotalText.setText("Total: " + limit + " MB.");
            }
            actionBarDateText.setText("Expire: " + sp.getString("dataDuration_both", "NO Limit"));

        }
    }

    public static float getPercentage(double n, long total) {
        float proportion = ((float) n) / ((float) total);
        return proportion * 100;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        StartAppAd.onBackPressed(this);
    }
}
