package triumphit.nets.speeds.meters;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Tushar on 1/23/2016.
 */
public class DownloadFile extends AsyncTask<String, String, String> {

    OnComplete oc;
    static SharedPreferences sp;
    static SharedPreferences.Editor editor;
    Context context;

    public DownloadFile(Context context, OnComplete oc) {
        this.context = context;
        sp = context.getSharedPreferences("stat", Context.MODE_PRIVATE);
        editor = sp.edit();
        this.oc = oc;
    }

    @Override
    protected String doInBackground(String... params) {
        int count;
        try {
            URL url = new URL(params[0]);
            URLConnection conexion = url.openConnection();
            conexion.connect();
            int lenghtOfFile = conexion.getContentLength();
            Log.e("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
            InputStream input = new BufferedInputStream(url.openStream());
            FileOutputStream fos = context.openFileOutput("download.doc", Context.MODE_PRIVATE);
            OutputStream output = fos;
            byte data[] = new byte[1024];

            long total = 0;

            while (sp.getBoolean("contDown", true) && ((count = input.read(data)) != -1)) {
                total += count;
                //publishProgress(""+(int)((total*100)/lenghtOfFile));
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String unused) {
        File f = new File(context.getFilesDir(), "download.doc");
        //f.delete();
        if(f.delete()){
            Log.e("Deleted", "Deleted");
            oc.onDownloadComplete("done");
        }
    }
}
